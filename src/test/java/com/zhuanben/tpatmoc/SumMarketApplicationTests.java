package com.zhuanben.tpatmoc;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhuanben.tpatmoc.config.RabbitMQConfig;
import com.zhuanben.tpatmoc.domain.Comment;
import com.zhuanben.tpatmoc.domain.Good;
import com.zhuanben.tpatmoc.domain.LoginUser;
import com.zhuanben.tpatmoc.domain.OrderDetail;
import com.zhuanben.tpatmoc.domain.dto.GoodItemDTO;
import com.zhuanben.tpatmoc.domain.dto.OrderDTO;
import com.zhuanben.tpatmoc.domain.po.OrderDetailPO;
import com.zhuanben.tpatmoc.mapper.GoodMapper;
import com.zhuanben.tpatmoc.mapper.OrderDetailMapper;
import com.zhuanben.tpatmoc.service.CommentService;
import com.zhuanben.tpatmoc.service.GoodMainImgService;
import com.zhuanben.tpatmoc.service.QueueMessageService;
import com.zhuanben.tpatmoc.util.DateUtils;
import com.zhuanben.tpatmoc.util.RedisUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@SpringBootTest
class SumMarketApplicationTests {

    @Autowired
    GoodMainImgService mainImgService;

    @Autowired
    CommentService commentService;

    @Autowired
    GoodMapper goodMapper;

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void contextLoads() {
        Date date = new Date();
        System.out.println(date.getTime());
    }



    @Test
    void testGetGoodItem(){
        List<GoodItemDTO> goodItemByCategoryId = goodMapper.getGoodItemByCategoryId(Long.valueOf(4));
        System.out.println(goodItemByCategoryId);
    }

    @Test
    void testClassClazz(){
        Object login_10001 = RedisUtils.get("login_10001", LoginUser.class);
        Object parse = JSON.parse(login_10001.toString(), LoginUser.class.getModifiers());

        System.out.println(parse);
    }

    @Autowired
    OrderDetailMapper orderDetailMapper;
    @Test
    void test(){
        OrderDetailPO orderDetailPO = new OrderDetailPO();
        orderDetailPO.setOrderId(Long.valueOf( 10040));
        List<OrderDetail> list = new ArrayList<>();
        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setSkuid(Long.valueOf(1003));
        orderDetail.setAmount(6);
        orderDetail.setOid(Long.valueOf(10400));
        orderDetail.setUnitPayment(BigDecimal.valueOf(1999.00));
        list.add(orderDetail);
        orderDetailPO.setList(list);
        orderDetailMapper.insertOrderDetailByOrderDetailPO(orderDetailPO);
    }

    @Test
    void test2() {
//        Comment c = Comment.builder().id(Long.valueOf(1001)).content("test comm").commLevel(Long.valueOf(5))
//                .uid(Long.valueOf(10001)).skuid(Long.valueOf(1003)).publishTime(new Date())
//                .orderDetailId(Long.valueOf("1584068865199968282")).build();
//        List<String> list = new ArrayList<>();
//        list.add("test3333");
//        list.add("fdfsdfs");
//        CommentDTO commentDTO = CommentDTO.builder()
//        .comment(c).commentImgList(list).build();
//        boolean oneComment = commentService.createOneComment(commentDTO);
//        System.out.println(oneComment);

    }

    @Test
    void testGetAllOrderinfo(){
        Long oid = Long.valueOf(10077);
        OrderDTO orderThoroughInfoByOrderId = orderDetailMapper.getOrderThoroughInfoByOrderId(oid);
        System.out.println(orderThoroughInfoByOrderId);
    }

    @Autowired
    private QueueMessageService RabbitMqQueueMessageService;

    @Test
    void RabbitMQTest() throws Exception {
        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("messageId", UUID.randomUUID().toString());
        messageMap.put("messageData","测试Test信息");
        messageMap.put("createTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        RabbitMqQueueMessageService.send(messageMap,
                RabbitMQConfig.EXCHANGE_TOPICS,
                RabbitMQConfig.ROUTING_KEY_ORDER_CANCEL);
    }

    @Test
    void RabbitMQTestDLX() throws Exception {
        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("orderId","10077");
        messageMap.put("createTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        RabbitMqQueueMessageService.sendOrderProcess(messageMap, RabbitMQConfig.ROUTING_KEY_ORDER_CANCEL, null);
    }


    @Test
    void testRedis(){
        RedisUtils.set("test:test2:tt", "3", RedisUtils.ONE_HOUR);
    }

    @Test
    void testTime(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        date.setTime(Long.valueOf("1670412500978"));
        System.out.println("look --->>>>>>>");
        System.out.println(simpleDateFormat.format(date));
    }

    @Test
    void testRandom(){
        Random randomNum = new Random();
        char[] chars = randomNum.toString().toCharArray();
        System.out.println(chars[chars.length-1]);
    }

    @Test
    void testInsertNewGood(){

    }
}

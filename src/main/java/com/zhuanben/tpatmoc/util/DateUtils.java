package com.zhuanben.tpatmoc.util;

import org.springframework.stereotype.Component;

import java.util.Calendar;

@Component
public class DateUtils {

    public static Long getTodayRemainSecondNum() {
        long current = System.currentTimeMillis();	//当前时间毫秒数
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        long tomorrowZero = calendar.getTimeInMillis();
        long remainSecond = (tomorrowZero - current) / 1000;
        return remainSecond;
    }
}

package com.zhuanben.tpatmoc.util;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Component
public class RedisUtils {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    private static RedisUtils redisUtils;

    private static final ObjectMapper objectMapper = new ObjectMapper();
    // 1分钟
    public static final Long ONE_MINUTE = 1000L*60;
    // 一小时
    public static final Long ONE_HOUR = ONE_MINUTE*60;

    // 一天
    public static final Long ONE_DAY = ONE_HOUR*24;

    // 一周
    public static final Long ONE_WEEK = ONE_DAY*7;

    // 一个月
    public static final Long ONE_MONTH = ONE_WEEK*4;






    /**
     * 初始化
     */
    @PostConstruct
    public void init() {
        redisUtils = this;
        redisUtils.stringRedisTemplate = this.stringRedisTemplate;
    }

    /**
     * 查询key，支持模糊查询
     *
     * @param key
     */
    public static Set<String> keys(String key) {
        return redisUtils.stringRedisTemplate.keys(key);
    }


    /**
     * 获取值
     * @param key
     */
    public static Object get(String key, Class clazz) {
            Object returnObj = null;
        try {
            String objString = redisUtils.stringRedisTemplate.opsForValue().get(key);
            if (!Objects.isNull(objString)){
                returnObj =JSON.parseObject(objString, clazz);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnObj;
    }

    public static String get(String key) {
        String objString = redisUtils.stringRedisTemplate.opsForValue().get(key);
        return objString;
    }

    /**
     * 设置值
     *
     * @param key
     * @param value
     */
    public static void set(String key, Object value) {
        try{
            redisUtils.stringRedisTemplate.opsForValue()
                    .set(key, JSON.toJSONString(value));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 设置值，并设置过期时间
     * @param key
     * @param value
     * @param expireSecond 过期时间，单位秒
     */
    public static void set(String key, Object value, Long expireSecond) {
        try{
            redisUtils.stringRedisTemplate.opsForValue()
                    .set(key, objectMapper.writeValueAsString(value), expireSecond, TimeUnit.SECONDS);
        }catch (JsonProcessingException e){
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /**
     * 删出key
     *
     * @param key
     */
    public static void delete(String key) {
        redisUtils.stringRedisTemplate.opsForValue().getOperations().delete(key);
    }

    /**
     * 设置对象
     *
     * @param key     key
     * @param hashKey hashKey
     * @param object  对象
     */
    public static void hset(String key, String hashKey, Object object) {
        redisUtils.stringRedisTemplate.opsForHash().put(key, hashKey, object);
    }

    /**
     * 设置对象
     *
     * @param key     key
     * @param hashKey hashKey
     * @param object  对象
     * @param expire  过期时间，单位秒
     */
    public static void hset(String key, String hashKey, Object object, Integer expire) {
        redisUtils.stringRedisTemplate.opsForHash().put(key, hashKey, object);
        redisUtils.stringRedisTemplate.expire(key, expire, TimeUnit.SECONDS);
    }

    /**
     * 设置HashMap
     *
     * @param key key
     * @param map map值
     */
    public static void hashSet(String key, HashMap<String, Object> map) {
        redisUtils.stringRedisTemplate.opsForHash().putAll(key, map);
    }

    /**
     * key不存在时设置值
     *
     * @param key
     * @param hashKey
     * @param object
     */
    public static void hashSetAbsent(String key, String hashKey, Object object) {
        redisUtils.stringRedisTemplate.opsForHash().putIfAbsent(key, hashKey, object);
    }

    /**
     * 获取Hash值
     *
     * @param key
     * @param hashKey
     * @return
     */
    public static Object hashGet(String key, String hashKey) {
        return redisUtils.stringRedisTemplate.opsForHash().get(key, hashKey);
    }

    /**
     * 获取key的所有值
     *
     * @param key
     * @return
     */
    public static Object hashGet(String key) {
        return redisUtils.stringRedisTemplate.opsForHash().entries(key);
    }

    /**
     * 删除key的所有值
     *
     * @param key
     */
    public static void deleteKey(String key) {
        redisUtils.stringRedisTemplate.opsForHash().getOperations().delete(key);
    }

    /**
     * 判断key下是否有值
     * @param key
     */
    public static Boolean hasKey(String key) {
        return redisUtils.stringRedisTemplate.opsForHash().getOperations().hasKey(key);
    }

    /**
     * 判断key和hasKey下是否有值
     *
     * @param key
     * @param hasKey
     */
    public static Boolean hasKey(String key, String hasKey) {
        return redisUtils.stringRedisTemplate.opsForHash().hasKey(key, hasKey);
    }


}

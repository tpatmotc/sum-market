package com.zhuanben.tpatmoc.util;

import com.zhuanben.tpatmoc.customizeException.TokenException.TokenInvalidException;
import com.zhuanben.tpatmoc.customizeException.TokenException.TokenTimeoutException;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.*;

/**
 * JWT工具类
 */
@Component
public class JwtUtils {

    @Autowired
    private RedisUtils redisUtils;

    // 一周
    public static final Long ONE_WEEK = 1000L*60*60*24*7;
    // 一天
    public static final Long ONE_DAY = 1000L*60*60*24;
    // 一小时
    public static final Long ONE_HOUR = 1000L*60*60;

    //设置秘钥明文
    public static final String JWT_KEY = "Radeon";

    public static String getUUID(){
        String token = UUID.randomUUID().toString().replaceAll("-", "");

        return token;
    }

    /**
     * 生成jwt
     * @param subject token中要存放的数据（json格式）
     * @return
     */
    public static String createJWT(String subject) {
        JwtBuilder builder = getJwtBuilder(subject, null, getUUID());// 设置过期时间
        return builder.compact();
    }

    /**
     * 生成jtw
     * @param subject token中要存放的数据（json格式）
     * @param ttlMillis token超时时间
     * @return
     */
    public static String createJWT(String subject, Long ttlMillis) {
        JwtBuilder builder = getJwtBuilder(subject, ttlMillis, getUUID());// 设置过期时间
        return builder.compact();
    }

    private static JwtBuilder getJwtBuilder(String subject, Long ttlMillis, String uuid) {
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        SecretKey secretKey = generalKey();
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        if(ttlMillis==null){
            ttlMillis= JwtUtils.ONE_HOUR;
        }
        long expMillis = nowMillis + ttlMillis;
        Date expDate = new Date(expMillis);
        return Jwts.builder()
                .setId(uuid)              //唯一的ID
                .setSubject(subject)   // 主题  可以是JSON数据
                .setIssuer("RD")     // 签发者
                .setIssuedAt(now)      // 签发时间
                .signWith(signatureAlgorithm, secretKey) //使用HS256对称加密算法签名, 第二个参数为秘钥
                .setExpiration(expDate);
    }

    /**
     * 创建token
     * @param id
     * @param subject
     * @param ttlMillis
     * @return
     */
    public static String createJWT(String id, String subject, Long ttlMillis) {
        JwtBuilder builder = getJwtBuilder(subject, ttlMillis, id);// 设置过期时间
        return builder.compact();
    }



    /**
     * 生成加密后的秘钥 secretKey
     * @return
     */
    public static SecretKey generalKey() {
        byte[] encodedKey = Base64.getDecoder().decode(JwtUtils.JWT_KEY);
        SecretKey key = new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");
        return key;
    }

    /**
     * 解析
     *
     * @param jwt
     * @return
     * @throws Exception
     */
    public static Claims parseJWT(String jwt) {
        SecretKey secretKey = generalKey();
        Claims claim = null;
        try{
            claim = Jwts.parser()
                    .setSigningKey(secretKey)
                    .parseClaimsJws(jwt)
                    .getBody();
        }catch (MalformedJwtException e){ // 处理token格式不正确
            throw new TokenInvalidException("token格式不合法");
        }catch (ExpiredJwtException e){ // 处理token过期
            throw new TokenTimeoutException("身份登录已经过期，请重新登录");
        }
        return claim;
    }

    public static Map<String,String> createDoubleToken(String subject){
        Map<String,String> tokenMap = new HashMap<>();
        String tempToken = createJWT(subject, ONE_HOUR/3);
        tokenMap.put("access_token", tempToken);
        tempToken  = createJWT(subject, ONE_WEEK);
        tokenMap.put("refresh_token", tempToken);
        return tokenMap;

    }

    public static boolean verifyTokenAndUidIsSame(Long uid){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object userIdFromSecurity = authentication.getPrincipal();
        return userIdFromSecurity.toString().equals(uid.toString());
    }


}

package com.zhuanben.tpatmoc.config;


import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.HashMap;
import java.util.Map;

/**
     Broker:它提供一种传输服务,它的角色就是维护一条从生产者到消费者的路线，保证数据能按照指定的方式进行传输,
     Exchange：消息交换机,它指定消息按什么规则,路由到哪个队列。
     Queue:消息的载体,每个消息都会被投到一个或多个队列。
     Binding:绑定，它的作用就是把exchange和queue按照路由规则绑定起来.
     Routing Key:路由关键字,exchange根据这个关键字进行消息投递。
     vhost:虚拟主机,一个broker里可以有多个vhost，用作不同用户的权限分离。
     Producer:消息生产者,就是投递消息的程序.
     Consumer:消息消费者,就是接受消息的程序.
     Channel:消息通道,在客户端的每个连接里,可建立多个channel.
 */

@Configuration
public class RabbitMQConfig {
    @Value("${spring.rabbitmq.host}")
    private String host;

    @Value("${spring.rabbitmq.port}")
    private int port;

    @Value("${spring.rabbitmq.username}")
    private String username;

    @Value("${spring.rabbitmq.password}")
    private String password;

    @Value("${spring.rabbitmq.virtual-host}")
    private String virtualHost;

    /* -----------------时间配置---------------------------*/

    public static final Long HALF_HOUR = 1000L*60*30;

    public static final Long ONE_DAY = 1000L*60*60*24;

    /* -----------------以下是交换机的配置------------------ */
    public static final String EXCHANGE_TOPICS = "mq_topics_exchange";
    public static final String EXCHANGE_RECEIVE_DLX = "mq_DLX_receive_exchange";
    public static final String EXCHANGE_DLX = "mq_DLX_exchange";
    public static final String EXCHANGE_DLX_AUTO_CONFIRM = "mq_DLX_auto_receive";
    public static final String EXCHANGE_RECEIVE_DLX_AUTO_CONFIRM = "mq_DLX_receive_exchange";



    /* -------------------以下是队列的配置-------------------- */
    public static final String QUEUE_A = "QUEUE_A";
    public static final String QUEUE_TEST = "QUEUE_TEST"; // 测试队列

    /* 本项目未使用该队列 */
    public static final String QUEUE_TTL = "QUEUE_TTL"; // TTL超时队列
    public static final String QUEUE_DLX_AUTO_CANCEL = "QUEUE_DLX_AUTO_CANCEL"; // 死信队列
    public static final String QUEUE_RECEIVE_DLX_AUTO_CANCEL = "QUEUE_RECEIVE_DLX_AUTO_CANCEL"; // 死信队列

    public static final String QUEUE_DLX_AUTO_CONFIRM = "QUEUE_DLX_AUTO_CONFIRM";

    public static final String QUEUE_RECEIVE_DLX_AUTO_CONFIRM = "QUEUE_RECEIVE_DLX_AUTO_CONFIRM";

    public static final String QUEUE_DLX_AUTO_REFRESH_RECOMMEND = "QUEUE_DLX_AUTO_REFRESH_RECOMMEND";

    public static final String QUEUE_RECEIVE_DLX_AUTO_REFRESH_RECOMMEND = "QUEUE_RECEIVE_DLX_AUTO_REFRESH_RECOMMEND";


    /* -------------------路由键的配置 -------------------- */
    /* 本项目已使用该路由键 */
    public static final String ROUTING_KEY_ORDER_CANCEL = "order.orderCancel";

    public static final String ROUTING_KEY_ORDER_AUTO_CONFIRM = "order.autoConfirm";

    public static final String ROUTING_KEY_AUTO_REFRESH_RECOMMEND = "recommend.auto.refresh";

    // 创建连接
    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(host,port);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        connectionFactory.setVirtualHost(virtualHost);
        //发布确认，template要求CachingConnectionFactory的publisherConfirms属性设置为true
        connectionFactory.setPublisherConfirms(true);
//        connectionFactory.setPublisherReturns(true);

        return connectionFactory;
    }

    //RabbitMQ的使用入口
    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    //必须是prototype类型
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate template = new RabbitTemplate(this.connectionFactory());
        template.setMessageConverter(this.jsonMessageConverter());
        template.setMandatory(true);
        return template;
    }

    //把交换机，队列，通过路由关键字进行绑定，写在RabbitConfig类当中
    /**
     * 针对消费者配置
     * 1. 设置交换机类型
     * 2. 将队列绑定到交换机
     FanoutExchange: 将消息分发到所有的绑定队列，无routingkey的概念
     HeadersExchange:通过添加属性key-value匹配
     DirectExchange:按照routingkey分发到指定队列
     TopicExchange:多关键字匹配
     */
    /* 本项目未使用该连接工厂，仅测试 */
/*
    @Bean
    public TopicExchange testTopicExchange() {
        return new TopicExchange(EXCHANGE_TOPICS);
    }
*/

    /**
     * 获取队列A
     * durable:是否持久化,默认是false,持久化队列：会被存储在磁盘上(ErLang内部DB)，当消息代理重启时仍然存在，暂存队列：当前连接有效
     * exclusive:默认也是false，只能被当前创建的连接使用，而且当连接关闭后队列即被删除。此参考优先级高于durable
     * autoDelete:是否自动删除，当没有生产者或者消费者使用此队列，该队列会自动删除。
     * return new Queue(QUEUE_TEST,true,true,false);
     * 一般设置一下队列的持久化就好,其余两个就是默认false
     * @return
     */
    /* 本项目未使用该队列，仅测试 */
/*
    @Bean
    public Queue queueA() {
        return new Queue(QUEUE_A, true); //队列持久
    }
*/

    /* 本项目未使用该队列，仅测试 */
/*
    @Bean
    public Queue queueTest(){
        return new Queue(QUEUE_TEST, true);//队列持久
    }
*/
    /* 本项目未使用该连接工厂，仅测试通过 */
/*
    @Bean // 将队列和交换机绑定, 并设置用于匹配键
    public Binding bindingTest() {
        return BindingBuilder.bind(queueTest()).to(testTopicExchange()).with(ROUTING_KEY_ORDER_CANCEL);
    }
*/
    /*  以下是关于订单 “自动取消” 死信队列跟TTL队列的配置--------------------------------------------------------------     */

    /* 本项目未使用该队列 */
    // 产生 TTL队列
//    @Bean
//    public Queue queueTTL() {
//        Map<String, Object> map = new HashMap<>();
//        map.put("x-message-ttl", 10000);
//        return new Queue(QUEUE_TTL, true, false, false, map);
//    }

    /**   >> 开始配置 订单超时自动取消的死信队列与交换机 ---------------->         */

    @Bean // 生成订单30分钟自动取消的死信队列
    public Queue createDlxOrderAutoCancelQueue() {
        Map<String, Object> props = new HashMap<>();
        props.put("x-message-ttl", HALF_HOUR);
        props.put("x-dead-letter-exchange", EXCHANGE_RECEIVE_DLX); // 配置死信接收交换机
        props.put("x-dead-letter-routing-key", ROUTING_KEY_ORDER_CANCEL);
        return new Queue(QUEUE_DLX_AUTO_CANCEL, true, false, false, props);
    }

    @Bean // 生成订单30分钟自动取消的死信交换机
    public TopicExchange exchangeDLX() {
        return new TopicExchange(EXCHANGE_DLX);
    }

    @Bean // 给上述订单自动取消死信队列绑定交换机
    public Binding bindingExchange2OrderAutoCancelDLX() {
        return BindingBuilder.bind(createDlxOrderAutoCancelQueue())
                                    .to(exchangeDLX())
                                    .with(ROUTING_KEY_ORDER_CANCEL);
    }
    /**   << 死信队列与交换机配置结束 ---------------- <         */

    /**   》》 死信“接收”队列与死信接收交换机配置开始 ----------------》》     */

    @Bean // 订单自动取消的 死信“接收”交换机
    public TopicExchange DlxReceiveExchange() {
        return new TopicExchange(EXCHANGE_RECEIVE_DLX);
    }
    @Bean // 订单自动取消的死信 “接收”队列
    public Queue createOrderCancelDlxReceiveQueue() {
        return new Queue(QUEUE_RECEIVE_DLX_AUTO_CANCEL);
    }

    @Bean // 将上述两个死信 “接收” 交换机与队列绑定
    public Binding bindingDlxReceiveExchangeAndQueue() {
        return BindingBuilder.bind(createOrderCancelDlxReceiveQueue())
                        .to(DlxReceiveExchange())
                        .with(ROUTING_KEY_ORDER_CANCEL);
    }
    /**   死信接收队列与死信接收交换机配置结束 ---------------- 《《    */


    /* 配置订单自动确认的交换机与队列------------------------------------------------------------------------------------------------- */
        /** >> 开始设置自动收货的 Queue 跟 Exchange -------------------->> */
    @Bean // 生成订单自动确认的队列
    public Queue DlxOrderAutoConfirmQueue() {
        Map<String, Object> props = new HashMap<>();
        // 测试阶段。10秒后，消息自动变为死信

        props.put("x-message-ttl", ONE_DAY*7);
//        props.put("x-message-ttl", HALF_HOUR/3*4); // 测试阶段，自动确认设为40分钟
        props.put("x-dead-letter-exchange", EXCHANGE_RECEIVE_DLX); // 配置死信接收交换机
        props.put("x-dead-letter-routing-key", ROUTING_KEY_ORDER_CANCEL);
        return new Queue(QUEUE_DLX_AUTO_CONFIRM, true, false, false, props);
    }
    @Bean // 生成订单自动确认的交换机
    public TopicExchange exchangeDlxAutoConfirmOrder() {
        return new TopicExchange(EXCHANGE_DLX_AUTO_CONFIRM);
    }
    @Bean // 将上述 交换机与队列进行绑定
    public Binding bindingDlx2AutoConfirm() {
        return BindingBuilder.bind(DlxOrderAutoConfirmQueue())
                .to(exchangeDlxAutoConfirmOrder())
                .with(ROUTING_KEY_ORDER_AUTO_CONFIRM);
    }
        /** << 配置结束-------------------------------<< */

        /** 》》 开始配置死信队列接收交换机----------------》》 */
    @Bean // 生成订单自动确认的交换机
    public TopicExchange DlxReceiveAutoConfirmExchange() {
        return new TopicExchange(EXCHANGE_RECEIVE_DLX_AUTO_CONFIRM);
    }

    @Bean // 生成订单自动确认的队列
    public Queue DlxReceiveAutoConfirmQueue() {
        return new Queue(QUEUE_RECEIVE_DLX_AUTO_CONFIRM);
    }

    @Bean // 绑定上述两个交换机
    public Binding bindingDlxReceiveAutoConfirmExchangeAndQueue() {
        return BindingBuilder.bind(DlxReceiveAutoConfirmQueue()).
                to(DlxReceiveExchange()).with(ROUTING_KEY_ORDER_AUTO_CONFIRM);
    }
        /** 《《 配置结束 ---------------------《《*/

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}

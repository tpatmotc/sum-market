package com.zhuanben.tpatmoc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.zhuanben.tpatmoc.domain.CommentImg;
import com.zhuanben.tpatmoc.domain.dto.CommentDTO;
import com.zhuanben.tpatmoc.domain.po.CommImgPO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface CommentImgMapper extends BaseMapper<CommentImg> {

    int insertCommImgList(CommImgPO commImgPO);
}

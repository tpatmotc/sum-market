package com.zhuanben.tpatmoc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.zhuanben.tpatmoc.domain.Good;
import com.zhuanben.tpatmoc.domain.dto.GoodDTO;
import com.zhuanben.tpatmoc.domain.dto.GoodItemDTO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface GoodMapper extends BaseMapper<Good> {

    List<GoodItemDTO> getGoodItemByCategoryId(Long cateId);

    GoodDTO getAllGoodDetailByGoodId(Long goodId);

     List<GoodItemDTO> searchByKeyword(String keyword);

    List<GoodItemDTO>  recommendYourLike();

    Long insertNewGood(Good good);
}

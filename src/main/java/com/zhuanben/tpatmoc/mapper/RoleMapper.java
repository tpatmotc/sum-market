package com.zhuanben.tpatmoc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhuanben.tpatmoc.domain.Role;
import com.zhuanben.tpatmoc.domain.SystemUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface RoleMapper extends BaseMapper<Role> {

    @Select("SELECT t_role.id,t_role.role_name from t_role\n" +
            "INNER JOIN t_user_role ON t_role.id = t_user_role.rid\n" +
            "INNER JOIN t_user On t_user.id = t_user_role.uid\n" +
            "where t_user.username = #{username} ")
    List<Role> getRoleByUser(String username);
}

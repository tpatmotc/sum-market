package com.zhuanben.tpatmoc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.zhuanben.tpatmoc.domain.SystemUser;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UserMapper extends BaseMapper<SystemUser> {

    int signIn(Long uid);

}

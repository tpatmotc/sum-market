package com.zhuanben.tpatmoc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zhuanben.tpatmoc.domain.Comment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CommentMapper extends BaseMapper<Comment> {

    @Select("select id,comment_level,uid,order_detail_id,content,publish_time from t_comment" +
            "where state = #{state}")
    List<Comment> selectPageVo(IPage<Comment> page, Integer state);

    int insertComment(Comment comment);
}

package com.zhuanben.tpatmoc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhuanben.tpatmoc.domain.Category;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface CategoryMapper extends BaseMapper<Category> {

}

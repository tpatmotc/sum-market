package com.zhuanben.tpatmoc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.zhuanben.tpatmoc.domain.GoodDetail;
import com.zhuanben.tpatmoc.domain.OrderDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface GoodDetailMapper extends BaseMapper<GoodDetail> {

    List<Integer> getStockListBySkuidList(List<Long> list);

    int steepStockByOrderDetailList(@Param("orderDetailList") List<OrderDetail> orderDetailList);

    int riseStockByGoodDetailList(@Param("goodDetailList") List<GoodDetail> goodDetailList);


}

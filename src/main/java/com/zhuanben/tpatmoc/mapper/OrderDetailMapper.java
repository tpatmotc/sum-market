package com.zhuanben.tpatmoc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.zhuanben.tpatmoc.domain.OrderDetail;
import com.zhuanben.tpatmoc.domain.dto.OrderDTO;
import com.zhuanben.tpatmoc.domain.po.OrderDetailPO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {

    /**
     *
     * @param orderDetailPO 包含
     *          property: list -> value: List<OrderDetail> 待插入的orderDetail数据
     *          property: orderId         -> value: orderId 这些数据所属的orderId
     * @return
     */
    int insertOrderDetailByOrderDetailPO(OrderDetailPO orderDetailPO);

    OrderDTO getOrderThoroughInfoByOrderId(Long orderId);

    int setUnusefulStateByDetailIdList(@Param("orderDetailIdList") List<Long> orderDetailIdList);
}

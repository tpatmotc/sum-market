package com.zhuanben.tpatmoc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.zhuanben.tpatmoc.domain.Order;
import com.zhuanben.tpatmoc.domain.dto.OrderDTO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface OrderMapper extends BaseMapper<Order> {

    Long createOneOrder(Order order);

    List<OrderDTO> getOrderByUid(Map map);
}

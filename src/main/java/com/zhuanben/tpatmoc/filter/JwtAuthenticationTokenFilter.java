package com.zhuanben.tpatmoc.filter;

import com.zhuanben.tpatmoc.customizeException.TokenException.TokenException;
import com.zhuanben.tpatmoc.domain.LoginUser;
import com.zhuanben.tpatmoc.util.JwtUtils;
import com.zhuanben.tpatmoc.util.RedisUtils;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    @Autowired
    RedisTemplate redisTemplate;

    /**
     * 检查请求信息的过滤器
     * @param request 请求头，体
     * @param response 响应头，体
     * @param filterChain
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws IOException, ServletException {
        // 1.获取token
        String token = request.getHeader("access_token");
        if(!StringUtils.hasText(token)){
            // 没有token直接放行，让后面的过滤器判断是否认证状态
            filterChain.doFilter(request,response);
            return;
        }
        // 2.解析token
        String uid = null;
        LoginUser loginUser = null;
            /* subject即是用于生成token的元数据*/
            try{
                Claims claims = JwtUtils.parseJWT(token);
                uid = claims.getSubject();
                loginUser = (LoginUser) RedisUtils.get("login_" + uid, LoginUser.class);
            }catch (TokenException e){ // 这里处理不合法的token格式
                request.setAttribute("tokenException", e);
                request.getRequestDispatcher("/exception/token").forward(request,response);
                return;
            }

        // 认证完成后的鉴权: 将 ‘getAuthorities()方法的返回值’填入LoginUser
        // 将这个用户的信息存入SecurityContextHolder安全上下文
        /**  principal：一般指的是用户名，
         *   credentials：一般指的是密码，
         *   authorities： 指权限
         *   使用3个参数的构造器，内部会直接把这个对象的认证状态设为已认证，方便后续过滤器跳过判断耗时。
         */
        // 传入的第三个参数应该是login实现类里面的getAuthorities()方法的返回值，不能直接用getter的返回值
        UsernamePasswordAuthenticationToken upAuthToken =
                new UsernamePasswordAuthenticationToken(loginUser.getSystemUser().getId(),loginUser.getPassword(),
                        loginUser.getAuthorities());
        System.out.println(upAuthToken);

        SecurityContextHolder.getContext().setAuthentication(upAuthToken);
        // 5. 加入filterChain放行
        filterChain.doFilter(request,response);
    }
}

package com.zhuanben.tpatmoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

/**
 * 启动须知：
 *  本项目需要RabbitMQ的支持，请先启动RabbitMQ服务，并手动创建名为“/order_process_VM”的RabbitMQ虚拟机（重要）
 *  使用MySQL8.0
 *  需要Redis服务的支持
 *  需要Maven模块的支持
 *  需要在application.properties（被git忽略）中手动书写“微信小程序”的“WECHAT_APPID” & “WECHAT_SECRET”
 */
@SpringBootApplication
public class SumMarketApplication {
    public static void main(String[] args) {
        SpringApplication.run(SumMarketApplication.class, args);
    }

}

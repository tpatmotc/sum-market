package com.zhuanben.tpatmoc.domain;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 【请填写功能名称】对象 t_category
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "t_category")
public class Category extends BaseEntity{

    /** $column.columnComment */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "商品分类id", required = false)
    private Long id;

    /** 分类等级 */
    @ApiModelProperty(value = "分类等级", required = true)
    private Long categoryLevel;

    /** 分类类型，填具体类型，eg: 一级分类--> 手机电视，二级分类->>Z系列，W系列...  目前最大支持3级分类 */
    @ApiModelProperty(value = "分类类型（如一级分类：手机/耳机，二级分类：note系列/s系列，最多支持三级分类）", required = true)
    private String categoryType;
    private Long parentId;

}

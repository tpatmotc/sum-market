package com.zhuanben.tpatmoc.domain.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CommImgPO {
    private Long commId;
    private List<String> commImgPathList;
}

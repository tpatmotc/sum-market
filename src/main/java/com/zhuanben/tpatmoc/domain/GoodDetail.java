package com.zhuanben.tpatmoc.domain;

import java.math.BigDecimal;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 【请填写功能名称】对象 t_good_detail
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "t_good_detail")
@Builder
public class GoodDetail extends BaseEntity{

    /** 商品skuid号，最小库存单位编码 */
    @TableId(value = "skuid", type = IdType.AUTO)
    @ApiModelProperty(value = "商品skuid号,最小库存单位编码", required = false)
    private Long skuid;

    /** $column.columnComment */
    @ApiModelProperty(value = "商品型号", required = true)
    private Long modelId;

    /** 商品所属号 */
    @ApiModelProperty(value = "商品所属号", required = true)
    private Long goodId;

    /** 价格 */
    @ApiModelProperty(value = "商品价格", required = true)
    private BigDecimal price;

    /** 自定义的商品参数 */
    @ApiModelProperty(value = "自定义的商品参数", required = true)
    private String info;

    /** 颜色 */
    @ApiModelProperty(value = "商品颜色", required = true)
    private String color;

    /** 存储类别 */
    @ApiModelProperty(value = "商品规格", required = true)
    private String storage;

    /** 库存数量 */
    @ApiModelProperty(value = "商品库存数量", required = true)
    private Integer stock;

    /** 状态，1有效（前端可以展示） 2删除（不展示） */
    @ApiModelProperty(value = "商品状态，1有效（前端可以展示） 2删除（不展示）", required = true)
    private Integer state;

    // 该注解表示该属性不属于这个类对应的表
    @TableField(exist = false)
    private String title;// 这个订单详情的商品title

    @TableField(exist = false)
    private String firstMainImgPath;// 这个GoodDetail的第一张主图路径



}

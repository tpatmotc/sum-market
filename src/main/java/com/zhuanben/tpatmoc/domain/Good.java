package com.zhuanben.tpatmoc.domain;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 【请填写功能名称】对象 t_good
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "t_good")
@Builder
public class Good extends BaseEntity{

    /** $column.columnComment */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id", required = false)
    private Long id;

    /** $column.columnComment */
    @ApiModelProperty(value = "商品名称", required = true)
    private String title;

    /** 一级分类 */
    @ApiModelProperty(value = "一级分类", required = true)
    private Long categoryLevelOne;

    /** 二级分类 */
    @ApiModelProperty(value = "二级分类", required = true)
    private Long categoryLevelTwo;

    /** 三级分类 */
    @ApiModelProperty(value = "三级分类", required = true)
    private Long categoryLevelThree;

    /** 状态 1:上架,2:下架,3:删除 */
    @ApiModelProperty(value = "商品状态（上架/下架/删除）", required = true)
    private Long state;

    @ApiModelProperty(value = "商品最低价",required = true)
    private BigDecimal lowestPrice;


}

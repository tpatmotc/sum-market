package com.zhuanben.tpatmoc.domain;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 【请填写功能名称】对象 t_role
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "t_role")
public class Role extends BaseEntity{

    /** $column.columnComment */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id", required = false)
    private Long id;

    /** 角色名 */
    @ApiModelProperty(value = "用户角色名", required = true)
    private String roleName;


}

package com.zhuanben.tpatmoc.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 订单对象 t_order
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@AllArgsConstructor
@Data
@NoArgsConstructor
@TableName(value = "t_order")
@Builder
public class Order extends BaseEntity{

    /** $column.columnComment */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id", required = false)
    private Long id;

    /** 实付金额 */
    @ApiModelProperty(value = "订单实际支付金额", required = true)
    private BigDecimal payment;

    /** 用户id */
    @ApiModelProperty(value = "", required = true)
    private Long uid;

    /** 收件信息id */
    @ApiModelProperty(value = "订单实际支付金额", required = true)
    private Long receiveId;

    /** 订单创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "订单创建时间", required = true)
    private Date createTime;

    /** 订单付款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "订单支付时间", required = true)
    private Date payTime;

    /** 订单结算时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "订单完成时间", required = true)
    private Date finishTime;

    @ApiModelProperty(value = "订单状态", required = true)
    private Integer state;
}

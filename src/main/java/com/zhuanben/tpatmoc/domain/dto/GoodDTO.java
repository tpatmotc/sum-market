package com.zhuanben.tpatmoc.domain.dto;

import com.zhuanben.tpatmoc.domain.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GoodDTO {
    Long goodId;
    String title;
    BigDecimal lowestPrice;
    List<GoodDetail> goodDetailList;
    List<String> mainImgList;
    List<String> introImgList;
    List<String> colorList;
    List<String> storageList;

}

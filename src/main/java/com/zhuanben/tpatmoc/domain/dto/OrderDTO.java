package com.zhuanben.tpatmoc.domain.dto;


import com.zhuanben.tpatmoc.domain.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderDTO {

    private Long orderId;

    private Long orderUid;

    private Long orderReceiveId;


    private String receiveAddress;

    private String receiveTel;

    private String receiveName;

    private String orderPayment;
    private Date orderCreateTime;
    private Date orderPayTime;
    private Date orderFinishTime;

    private String createTimeString;
    private String payTimeString;
    private String finishTimeString;

    private Integer orderState;

    private Integer starPoint;

    // 发送订单时使用
    private List<OrderDetail> orderDetailList;

    // 获取订单信息时使用的图片路径
    private List<String> goodImgPathList;
}

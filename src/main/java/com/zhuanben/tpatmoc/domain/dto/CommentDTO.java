package com.zhuanben.tpatmoc.domain.dto;

import com.zhuanben.tpatmoc.domain.Comment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CommentDTO {

    private Long skuid;

    private Long uid;

    private Long commLevel;

    private String content;

    private String publishTime;

    private String orderDetailId;

    private List<String> commentImgList;

    private String nickname;

    private String avatar;

}

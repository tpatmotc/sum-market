package com.zhuanben.tpatmoc.domain.dto;

import com.zhuanben.tpatmoc.domain.SystemUser;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
    public class SystemUserDTO {
    private SystemUser systemUser;
    private String jsCode;
    private boolean isSignToday;
}

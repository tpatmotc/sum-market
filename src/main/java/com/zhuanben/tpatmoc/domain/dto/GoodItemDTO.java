package com.zhuanben.tpatmoc.domain.dto;

import com.zhuanben.tpatmoc.domain.Good;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GoodItemDTO {

    private Good good;
    private String mainImgPath;

}

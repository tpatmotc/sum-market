package com.zhuanben.tpatmoc.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Data
public class LoginUser extends BaseEntity implements UserDetails  {

    private SystemUser systemUser;

    private List<String> permissions;

    public LoginUser(SystemUser systemUser, List<String> permissions) {
        this.systemUser = systemUser;
        this.permissions = permissions;
    }
    public LoginUser(){
    }

    @JSONField(serialize = false)
    private List<SimpleGrantedAuthority> sgAuthList;

    /**
     * @author 余志豪
     * @return sgAuthList 包含权限信息的列表
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if(Objects.isNull(permissions)){
            return sgAuthList;
        }
        // 把permission中String类型的权限信息封装成SimpleGrantedAuthority对象
        // 将String权限对象封装为SimpleGrantedAuthority对象转入容器并返回
        sgAuthList
                = permissions.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
        return sgAuthList;
    }

    @Override
    public String getPassword() {
        return systemUser.getPassword();
    }

    @Override
    public String getUsername() {
        return systemUser.getUsername();
    }

    // 是否未过期
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    // 是否没有超时
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    // 是否可用
    @Override
    public boolean isEnabled() {
        return true;
    }
}


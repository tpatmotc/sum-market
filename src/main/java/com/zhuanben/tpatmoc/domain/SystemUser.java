package com.zhuanben.tpatmoc.domain;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 【请填写功能名称】对象 t_user
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "t_user")
@ApiModel(value = "user对象",description = "用于登录系统的user用户类")
@Builder
public class SystemUser extends BaseEntity{

    /** $column.columnComment */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "用户的编号id", required = false)
    private Long id;

    /** $column.columnComment */
    @ApiModelProperty(value = "用户的登录用户名username", required = true)
    private String username;

    /** 加密后的字符 */
    @ApiModelProperty(value = "用户的登录密码", required = true)
    private String password;

    /** $column.columnComment */
    @ApiModelProperty(value = "用户的电话号码", required = true)
    private String tel;

    @ApiModelProperty(value = "用户头像地址")
    private String avatar;

    @ApiModelProperty(value = "用户昵称")
    private String nickname;

    private String openId;

    private Integer starPoint;
}

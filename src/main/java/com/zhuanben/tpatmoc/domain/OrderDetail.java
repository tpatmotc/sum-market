package com.zhuanben.tpatmoc.domain;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 【请填写功能名称】对象 t_order_detail
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "t_order_detail")
@Builder
public class OrderDetail extends BaseEntity{

    /** $column.columnComment */
    private Long id;

    /** 所属父订单id */

    private Long oid;

    /** $column.columnComment */

    private Long skuid;

    /** 数量 */

    private Integer amount;

    /** 实付价格 */

    private BigDecimal unitPayment;

    /** 1:未付款,2:已付款待发货,3:取消订单,4:交易完成,5:交易失败(try-catch到异常将当前oid全部置为5) */

    private Long state;

//    @TableField(exist = false)
//    private GoodDetail goodDetail;

    @TableField(exist = false)
    private String goodTitle;// 这个订单详情的商品title

    @TableField(exist = false)
    private String firstMainImgPath; // 这个OrderDetail的第一张主图路径

    @TableField(exist = false)
    private String color; // 这个订单详情的颜色

    @TableField(exist = false)
    private String storage; // 这个订单详情选择的规格

    @TableField(exist = false)
    private String idStr; // 为了解决前端得到原本的orderDetailIdStr时会丢精度的问题

    @TableField(exist = false)
    private Boolean isCommented; // 该商品是否已经评价过了

}

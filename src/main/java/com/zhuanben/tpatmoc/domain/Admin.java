package com.zhuanben.tpatmoc.domain;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 【请填写功能名称】对象 t_admin
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "t_admin")
@Deprecated
public class Admin extends BaseEntity {

    /** $column.columnComment */
    @TableId(type = IdType.AUTO)
    private Long id;

    /** $column.columnComment */
    private String username;

    /** 加密后的字符 */
    private String password;

    /** admin’s tel不得为null */
    private String tel;

}

package com.zhuanben.tpatmoc.domain;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 【请填写功能名称】对象 t_good_intro_img
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "t_good_intro_img")
@Builder
public class GoodIntroImg extends BaseEntity{

    /** $column.columnComment */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "商品详情页图片id", required = false)
    private Long id;

    /** $column.columnComment */
    @ApiModelProperty(value = "商品所属号", required = true)
    private Long gid;

    /** $column.columnComment */
    @ApiModelProperty(value = "商品详情页图片路径", required = true)
    private String imgPath;

    /** 状态，1启用，0废弃 */
    @ApiModelProperty(value = "状态（1启用/0废弃）", required = true)
    private Long state;


}

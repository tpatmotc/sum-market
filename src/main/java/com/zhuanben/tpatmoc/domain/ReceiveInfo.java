package com.zhuanben.tpatmoc.domain;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 收件地址对象 t_receive_info
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "t_receive_info")
public class ReceiveInfo extends BaseEntity{

    /** $column.columnComment */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "配送订单id", required = false)
    private Long id;

    /** 收件人地址 */
    @ApiModelProperty(value = "收件人地址", required = true)
    private String receiveAddress;

    /** 收件人姓名 */
    @ApiModelProperty(value = "收件人姓名", required = true)
    private String receiveName;

    /** 收件人tel */
    @ApiModelProperty(value = "收件人电话", required = true)
    private String receiveTel;

    /** 收件信息所属用户（谁创造的该收件信息） */
    @ApiModelProperty(value = "收件人账号id", required = true)
    private Long uid;

    /** 1启用，2被删除（前端不展示） */
    @ApiModelProperty(value = "该收件信息状态（启用/被删除）", required = true)
    private Integer state;


}

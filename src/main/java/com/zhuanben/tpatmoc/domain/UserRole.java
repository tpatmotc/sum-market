package com.zhuanben.tpatmoc.domain;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户角色对应对象 t_user_role
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "t_user_role")
public class UserRole extends BaseEntity{

    /** $column.columnComment */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "账号id", required = true)
    private Long uid;

    /** $column.columnComment */
    @ApiModelProperty(value = "账号评级编号", required = true)
    private Long rid;


}

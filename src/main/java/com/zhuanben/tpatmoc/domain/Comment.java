package com.zhuanben.tpatmoc.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * 【请填写功能名称】对象 t_comment
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "t_comment")
@Builder
public class Comment extends BaseEntity{

    /**
     * $column.columnComment
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id", required = false)
    private Long id;

    /**
     * 评论等级，好中差评
     */
    @ApiModelProperty(value = "评论等级（好/中/差）1-5", required = false)
    private Long commLevel;

    /**
     * 发表评论的用户id
     */
    @ApiModelProperty(value = "发表评论的用户id", required = true)
    private Long uid;

    /**
     * 商品的skuid,指向具体某个型号规格的产品
     */
    @ApiModelProperty(value = "商品skuid", required = true)
    private Long skuid;

    /**
     * 订单的id
     */
    @ApiModelProperty(value = "订单id", required = true)
    private Long orderDetailId;

    /**
     * 内容
     */
    @ApiModelProperty(value = "评论详情内容", required = false)
    private String content;

    /**
     * 发表时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "评论发表时间", required = true)
    private Date publishTime;

    /**
     * 1可见，2被删除
     */
//    @ApiModelProperty(value = "评论状态（1可见/2被删除)", required = false)


}


package com.zhuanben.tpatmoc.domain;

import com.alibaba.fastjson.JSON;
import lombok.Data;

@Data
public class ApiResult {

    public static final Integer OK = 200;
    public static final Integer UNAUTHORIZED = 401;

    public static final Integer FORBIDDEN = 403;

    public static final Integer SERVER_ERROR = 500;

    public static final Integer CLIENT_ERROR = 700;

    // 响应业务状态
    private Integer status;

    // 响应消息
    private String msg;

    // 响应中的数据
    private Object data;


    public static ApiResult build(Integer status, String msg, Object data) {
        return new ApiResult(status, msg, data);
    }

    public static ApiResult success(Object data) {
        return new ApiResult(ApiResult.OK, "OK", JSON.toJSONString(data));
    }

    public static ApiResult errorMsg(String msg) {
        return new ApiResult(ApiResult.SERVER_ERROR, msg, null);
    }

    public static ApiResult errorMsg(String msg, Integer status) {
        return new ApiResult(status, msg, null);
    }

    public static ApiResult errorMap(Object data) {
        return new ApiResult(600, "error", data);
    }

    public ApiResult() {

    }

    public ApiResult(Integer status, String msg, Object data) {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }


    public ApiResult(Object data) {
        this.status = 200;
        this.msg = "OK";
        this.data = data;
    }


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }


}

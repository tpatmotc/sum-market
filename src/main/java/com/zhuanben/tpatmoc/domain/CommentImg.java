package com.zhuanben.tpatmoc.domain;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 【请填写功能名称】对象 t_comment_img
 * 
 * @author ruoyi
 * @date 2022-10-10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "t_comment_img")
@Builder
public class CommentImg extends BaseEntity{

    /** $column.columnComment */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id", required = false)
    private Long id;

    /** 评论图片所属comm_id */
    @ApiModelProperty(value = "评论图片所属comm_id", required = true)
    private Long commId;

    /** 评论图片地址 */
    @ApiModelProperty(value = "评论图片地址", required = false)
    private String imgPath;
}

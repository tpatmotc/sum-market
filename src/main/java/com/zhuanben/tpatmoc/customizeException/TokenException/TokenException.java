package com.zhuanben.tpatmoc.customizeException.TokenException;

import com.zhuanben.tpatmoc.customizeException.BaseException;

public class TokenException extends BaseException {
    public TokenException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public TokenException(String msg) {
        super(msg);
    }
}

package com.zhuanben.tpatmoc.customizeException.TokenException;

import com.zhuanben.tpatmoc.customizeException.TokenException.TokenException;

public class TokenInvalidException extends TokenException {
    public TokenInvalidException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public TokenInvalidException(String msg) {
        super(msg);
    }
}

package com.zhuanben.tpatmoc.customizeException.TokenException;

public class TokenTimeoutException extends TokenException{
    public TokenTimeoutException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public TokenTimeoutException(String msg) {
        super(msg);
    }
}

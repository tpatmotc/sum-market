package com.zhuanben.tpatmoc.customizeException;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author 余志豪
 * 全局异常处理机制的基类，继承于RuntimeException
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BaseException extends RuntimeException{
    public BaseException() {
        super();
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseException(String message) {
        super(message);
    }

    public BaseException(Throwable cause) {
        super(cause);
    }

}

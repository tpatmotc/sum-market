package com.zhuanben.tpatmoc.customizeException.PurchaseException;

import com.zhuanben.tpatmoc.customizeException.BaseException;

public class PurchaseException extends BaseException {
    public PurchaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public PurchaseException(String message) {
        super(message);
    }
}

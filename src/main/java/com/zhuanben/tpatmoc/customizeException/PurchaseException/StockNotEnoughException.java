package com.zhuanben.tpatmoc.customizeException.PurchaseException;

import com.zhuanben.tpatmoc.customizeException.BaseException;

/**
 *  商品库存不足Exception
 */
public class StockNotEnoughException extends PurchaseException {
    public StockNotEnoughException(String msg) {
        super(msg);
    }
}

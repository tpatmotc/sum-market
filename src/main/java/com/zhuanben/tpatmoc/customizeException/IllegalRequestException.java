package com.zhuanben.tpatmoc.customizeException;

public class IllegalRequestException extends BaseException{
    public IllegalRequestException(String msg) {
    }

    public IllegalRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalRequestException() {

    }
}

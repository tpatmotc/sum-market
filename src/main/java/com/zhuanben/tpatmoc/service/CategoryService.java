package com.zhuanben.tpatmoc.service;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhuanben.tpatmoc.domain.Category;
import com.zhuanben.tpatmoc.mapper.CategoryMapper;
import com.zhuanben.tpatmoc.util.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Service
public class CategoryService {
    @Autowired
    CategoryMapper categoryMapper;

    @Autowired
    RedisTemplate redisTemplate;

    /**
     * @author 余志豪
     * @return List<Category> 包含全部一级分类的List对象
     */
    public List<Category> getCategoryOne() {
        List<Category> levelOneCategoryList =
                (List<Category>) RedisUtils.get("categoryLevelOne", List.class);
        if (Objects.isNull(levelOneCategoryList)){
            QueryWrapper<Category> categoryOneWrapper = new QueryWrapper<>();
            categoryOneWrapper.eq("category_level",1);
            levelOneCategoryList = categoryMapper.selectList(categoryOneWrapper);
            // 将一级分类存入redis，有效期是24小时
            RedisUtils.set("categoryLevelOne", levelOneCategoryList, RedisUtils.ONE_DAY);
        }
        return levelOneCategoryList;
    }

    /**
     * @author 余志豪
     * @param parentCategoryId 传入的父级分类id
     * @return 根据父级分类查询出来的所有子级分类数据
     */
    public List<Category> getChildCategory(int parentCategoryId) {
        List<Category> childCategoryList =
                (List<Category>) RedisUtils.get("childCategory_"+parentCategoryId, List.class) ;
        if(Objects.isNull(childCategoryList)){
            QueryWrapper<Category> childCategoryWrapper = new QueryWrapper<>();
            childCategoryWrapper.eq("parent_id", parentCategoryId);
            childCategoryList = categoryMapper.selectList(childCategoryWrapper);
            RedisUtils.set("childCategory_"+parentCategoryId, childCategoryList, RedisUtils.ONE_DAY);
        }
        return childCategoryList;
    }
}

package com.zhuanben.tpatmoc.service;

import com.alibaba.fastjson.JSON;
import com.zhuanben.tpatmoc.domain.LoginUser;
import com.zhuanben.tpatmoc.domain.SystemUser;
import com.zhuanben.tpatmoc.service.Impl.UserDetailsServiceImpl;
import com.zhuanben.tpatmoc.util.JwtUtils;
import com.zhuanben.tpatmoc.util.RedisUtils;
import io.jsonwebtoken.Claims;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
public class TokenService {

    @Resource
    UserDetailsServiceImpl userDetailsServiceImpl;

    @Resource
    SystemUserService systemUserService;
    
    public Map refreshNewToken(String refreshToken){
        refreshToken = (String) JSON.parseObject(refreshToken, Map.class).get("refreshToken");
//        System.out.println(refreshToken);
        Map<String,String> tokenMap = new HashMap<>();

        // parseJWT里面已经做了超时判断，不需要对refresh_token再次做判断
        Claims claims = JwtUtils.parseJWT(refreshToken);
//        System.out.println(claims.getSubject());
        // 能走到这说明没有超时，进行刷新
        String newAccessToken = JwtUtils.createJWT(claims.getSubject(), JwtUtils.ONE_HOUR);
        tokenMap.put("access_token", newAccessToken);
//        System.out.println( "这是 claims->>>>"+claims.getSubject());

        // 查看redis有没有这个systemUser数据
        SystemUser systemUser = (SystemUser) RedisUtils.get("user_" + claims.getSubject(), SystemUser.class);
        // redis没有就去SQL查询
        if(Objects.isNull(systemUser)){
            systemUser = systemUserService.getSystemUserByUid(Long.valueOf(claims.getSubject()));
        }

        UserDetails userDetails = userDetailsServiceImpl.loadUserByUsername(systemUser.getUsername());
        LoginUser loginUser = (LoginUser) userDetails;
        RedisUtils.set("login_"+loginUser.getSystemUser().getId(),loginUser, JwtUtils.ONE_HOUR);


        // 获取超时 时间的long值
        Long leftOverTime = claims.getExpiration().getTime();
        // 判断refresh_token剩余时长是否小于等于 access_token的两倍,如果是，则把refresh_token也刷新
        if (leftOverTime - ( new Date().getTime() ) <= JwtUtils.ONE_HOUR * 2){
//            System.out.println("access_token快过期开始刷新");
            RedisUtils.set("user_"+systemUser.getId(), systemUser, JwtUtils.ONE_DAY);
            String newRefreshToken = JwtUtils.createJWT(claims.getSubject(), JwtUtils.ONE_WEEK);
            tokenMap.put("refresh_token", newRefreshToken);
        }
        return tokenMap;
    }
}

package com.zhuanben.tpatmoc.service.Impl;

import com.alibaba.fastjson.JSON;
import com.zhuanben.tpatmoc.config.RabbitMQConfig;
import com.zhuanben.tpatmoc.service.OrderDetailService;
import com.zhuanben.tpatmoc.service.OrderService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.xml.crypto.Data;
import java.util.Date;
import java.util.Map;


/**
 * 配置消费者的Service类
 */
@Service
public class QueueMessageConsumer {

    @Resource
    OrderService orderService;

    @Resource
    OrderDetailService orderDetailService;
/*
    @RabbitListener(queues = RabbitMQConfig.QUEUE_TEST)
    @RabbitHandler
    public void consumeMessage(Message message){
//        log.info("收到的消息:{}",message);
        System.out.println("QueueMessageConsumer--收到的消息"+ message.toString());
    }
*/

    /** 将订单加入30分钟未付款自动取消
     *  @param message
     */
    @RabbitListener(queues = RabbitMQConfig.QUEUE_RECEIVE_DLX_AUTO_CANCEL)
    @RabbitHandler
    public void consumerOrderCancel(Message message){
        String msg =  new String(message.getBody());
        Map map = JSON.parseObject(msg, Map.class);
        Long orderId = Long.valueOf(map.get("orderId").toString()) ;

        Integer orderState = orderService.getOrderStateByOrderId(orderId);
        if(orderState == 1){
            orderService.cancelOrderByOrderId(orderId);
        }
    }
    @RabbitListener(queues = RabbitMQConfig.QUEUE_RECEIVE_DLX_AUTO_CONFIRM)
    @RabbitHandler
    public void consumerOrderAutoConfirm(Message message){
        String msg =  new String(message.getBody());
        Map map = JSON.parseObject(msg, Map.class);
        Long orderId = Long.valueOf(map.get("orderId").toString()) ;

        Integer orderState = orderService.getOrderStateByOrderId(orderId);
        if (orderState == 2) {
            orderService.confirmReceiveByOrderId(orderId, new Date().getTime());
        }
    }


}

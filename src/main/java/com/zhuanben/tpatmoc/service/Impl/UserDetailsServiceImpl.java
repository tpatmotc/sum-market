package com.zhuanben.tpatmoc.service.Impl;



import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhuanben.tpatmoc.domain.LoginUser;
import com.zhuanben.tpatmoc.domain.Role;
import com.zhuanben.tpatmoc.domain.SystemUser;
import com.zhuanben.tpatmoc.mapper.UserMapper;
import com.zhuanben.tpatmoc.service.RoleService;
import com.zhuanben.tpatmoc.service.SystemUserService;
import com.zhuanben.tpatmoc.util.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

//UserDetailsService是spring Security提供的用于封装认证用户信息的接口
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private RoleService roleService;

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    UserMapper userMapper;

    @Override
    /**
     * 该方法的作用是查询数据库中用户为name的用户信息，将数据库查询到的用户名、用户密码和用户权限封装在UserDetails中。
     */
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        // 通过自定义的方法获取用户及权限信息
        SystemUser systemUser = getSystemUserByUsername(name);

        if (systemUser != null) {
            // 存用户的信息，只保留一天
            RedisUtils.set("user_"+systemUser.getId(), systemUser,RedisUtils.ONE_DAY/2);
            List<String> roleList = roleService.getRoleByUid(name)
                    .stream().map(Role::getRoleName)
                    .collect(Collectors.toList());
            // LoginUser类内部实现了UserDetails所以可以直接return LoginUser
            return new LoginUser(systemUser, roleList);
        } else {
            // 如果查询的用户不存在（用户名不存在），抛出此异常
            throw new UsernameNotFoundException("当前用户不存在！");
        }
    }

    /**
     *
     * @author 余志豪
     * @param name 用户名
     * @return SystemUser的实例对象
     * @date 2022-10-14 10:55
     */
    public SystemUser getSystemUserByUsername(String name) {
        // 先查看redis里面有没有数据
        SystemUser systemUser = null;
        systemUser = (SystemUser) RedisUtils.get("user_"+name, SystemUser.class);
        if(!Objects.isNull(systemUser)){
            return  systemUser;
        }
        // 此时redis里面没数据，去DB查
        QueryWrapper<SystemUser> userWrapper = new QueryWrapper<>();
        userWrapper.eq("username",name);
        systemUser =  userMapper.selectOne(userWrapper);

        return systemUser;
    }

    public SystemUser getSystemUserByOpenId(String openid){
        SystemUser systemUser = (SystemUser) RedisUtils.get("user_"+openid, SystemUser.class);
        if(Objects.isNull( systemUser)) {
            QueryWrapper<SystemUser> systemUserQueryWrapper = new QueryWrapper<>();
            systemUserQueryWrapper.eq("open_id", openid);
            systemUser = userMapper.selectOne(systemUserQueryWrapper);

        }
        return systemUser;
    }
}

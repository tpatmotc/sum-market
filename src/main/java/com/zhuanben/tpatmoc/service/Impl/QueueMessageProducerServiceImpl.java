package com.zhuanben.tpatmoc.service.Impl;

import com.zhuanben.tpatmoc.service.QueueMessageService;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * 测试rabbitMQ使用的ServiceImpl类
 */
@Service
public class QueueMessageProducerServiceImpl implements QueueMessageService {
 
    //由于配置类RabbitMqConfig配置类中的rabbitTemplate的scope属性设置为ConfigurableBeanFactory.SCOPE_PROTOTYPE
    private RabbitTemplate rabbitTemplate;
 
    @Autowired
    public QueueMessageProducerServiceImpl(RabbitTemplate rabbitTemplate){
        this.rabbitTemplate = rabbitTemplate;
        //设置回调为当前类对象
        rabbitTemplate.setConfirmCallback(this);
    }
 
    @Override
    public void send(Object message, String exchange, String queueRoutingKey) throws Exception {
        //构建回调id为uuid
        String callBackId = UUID.randomUUID().toString();
        CorrelationData correlationId = new CorrelationData(callBackId);
//        log.info("开始发送消息内容:{}",message.toString());
        System.out.println("QueueMessageProducerServiceImpl--开始发送消息"+ message.toString());
        //发送消息到消息队列
        rabbitTemplate.convertAndSend(exchange, queueRoutingKey, message, correlationId);
//        log.info("发送定制的回调id:{}",callBackId);
        System.out.println("QueueMessageProducerServiceImpl--发送消息后的回调"+ callBackId);
    }

    @Override
    public void sendOrderProcess(Object message, String routingKey,String exchange) throws Exception{
//        String callBackId = JwtUtils.getUUID();
//        CorrelationData correlationId = new CorrelationData(callBackId);
        rabbitTemplate.convertAndSend(exchange,
                routingKey, message);
    }


    /**
     * 消息回调确认方法
     * @param correlationData 请求数据对象
     * @param ack 是否发送成功
     * @param s
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String s) {
        System.out.println("QueueMessageProducerServiceImpl-- 回调id:" + correlationData.getId());
        if (ack) {
            System.out.println("QueueMessageProducerServiceImpl--消息发送成功");
        } else {
            System.out.println("QueueMessageProducerServiceImpl--消息发送失败:" + s);
        }
    }
}
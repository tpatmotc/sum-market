package com.zhuanben.tpatmoc.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhuanben.tpatmoc.domain.GoodMainImg;
import com.zhuanben.tpatmoc.mapper.GoodMainImgMapper;
import com.zhuanben.tpatmoc.util.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Service
public class GoodMainImgService{
    @Autowired
    GoodMainImgMapper goodMainImgMapper;


    /**
     * @author 余志豪
     * @param goodId 商品id
     * @return 商品主图的网络地址
     */
    public String getOneMainImgByGoodId(Long goodId){
        String imgPath = (String) RedisUtils.get("goodOneMainImg_" + goodId, String.class);
        if(Objects.isNull(imgPath)){
            QueryWrapper<GoodMainImg> mainImgQueryWrapper = new QueryWrapper<>();
            mainImgQueryWrapper.eq("gid", goodId);
            mainImgQueryWrapper.orderByAsc("id");
            mainImgQueryWrapper.last("limit 1");// 通过限制一条数据，仅仅获取第一张图
            GoodMainImg goodMainImg = goodMainImgMapper.selectOne(mainImgQueryWrapper);
            imgPath = goodMainImg.getImgPath();
            RedisUtils.set("goodOneMainImg_" + goodId, imgPath,RedisUtils.ONE_MONTH);
        }
        return imgPath;
    }

    /**
     * 获取全部主图
     * @param goodId
     * @return 包含全部主图Object的List
     */
    public List<GoodMainImg> getAllGoodMainImg(Long goodId){
        QueryWrapper<GoodMainImg> mainImgQueryWrapper = new QueryWrapper<>();
        mainImgQueryWrapper.eq("gid", goodId);
        List<GoodMainImg> goodMainImgs = goodMainImgMapper.selectList(mainImgQueryWrapper);
        return  goodMainImgs;
    }
}

package com.zhuanben.tpatmoc.service;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhuanben.tpatmoc.domain.Good;
import com.zhuanben.tpatmoc.domain.GoodDetail;
import com.zhuanben.tpatmoc.domain.GoodIntroImg;
import com.zhuanben.tpatmoc.domain.GoodMainImg;
import com.zhuanben.tpatmoc.domain.dto.GoodDTO;
import com.zhuanben.tpatmoc.domain.dto.GoodItemDTO;
import com.zhuanben.tpatmoc.mapper.GoodDetailMapper;
import com.zhuanben.tpatmoc.mapper.GoodIntroImgMapper;
import com.zhuanben.tpatmoc.mapper.GoodMainImgMapper;
import com.zhuanben.tpatmoc.mapper.GoodMapper;
import com.zhuanben.tpatmoc.util.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
public class GoodService {
    @Autowired
    GoodMapper goodMapper;

    @Autowired
    GoodMainImgMapper goodMainImgMapper;

    @Autowired
    GoodIntroImgMapper goodIntroImgMapper;

    @Autowired
    GoodDetailMapper goodDetailMapper;

    /**
     * @author 获取的是good的主图，最低价，title数据
     * @param goodId
     * @return
     */
    public GoodDTO getGoodDetailByGoodId(Long goodId) {
        // 先去redis查询是否有这个对象
        GoodDTO goodDTO = (GoodDTO) RedisUtils.get("goodDTO_"+goodId, GoodDTO.class);
        if (Objects.isNull(goodDTO)){
            goodDTO = goodMapper.getAllGoodDetailByGoodId(goodId);
//            Map<String , List<GoodDetail>> goodDetailMap = new HashMap<>();
//            List<GoodDetail> goodDetailList = goodDTO.getGoodDetailList();
            // 创建颜色分类的数组
//            for (GoodDetail goodDetail: goodDetailList) {
//                String color = goodDetail.getColor();
//                if(goodDetailMap.get(color) != null){
//                   goodDetailMap.get(color).add(goodDetail);
//                }else {
//                    List<GoodDetail> tempList = new ArrayList<>();
//                    tempList.add(goodDetail);
//                    goodDetailMap.put(color, tempList);
//                }
//            }
            RedisUtils.set("goodDTO_"+goodId, goodDTO,RedisUtils.ONE_HOUR);
//            return goodDTO;
        }
        return goodDTO;
    }

    public List<GoodItemDTO> getGoodByCategoryId(Long cateId) {

        List<GoodItemDTO> goodItemList = (List<GoodItemDTO>) RedisUtils.get("category_item_"+cateId, List.class);
        if(Objects.isNull(goodItemList)){
            goodItemList = goodMapper.getGoodItemByCategoryId(cateId);
        }
        return goodItemList;
    }

    public List<GoodItemDTO> searchByKeyword(String keyword) {
        keyword = "%"+keyword+"%";
        List<GoodItemDTO> goodItemDtoList = goodMapper.searchByKeyword(keyword);
        return goodItemDtoList;
    }

    public List<GoodItemDTO> recommendYourLike() {
        int randomNum = new Random().nextInt();
        randomNum %= 10;
        List<GoodItemDTO> goodItemDTOList = (List<GoodItemDTO>) RedisUtils.get("recommendGood:recom_"+randomNum, List.class);
        if(goodItemDTOList == null){
            goodItemDTOList = goodMapper.recommendYourLike();
            RedisUtils.set("recommendGood:recom_"+randomNum,goodItemDTOList,RedisUtils.ONE_HOUR/6);
        }
        return goodItemDTOList;
    }

    /**
     *
     * @param good 填写title ， 1，2，3级分类即可
     * @param goodDetailList 填写price color storage stock
     * @param goodMainImgList 图片地址字符串
     * @param goodInfoImgList 如上
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertGoodText(Good good, List<GoodDetail> goodDetailList,List<String> goodMainImgList,
                              List<String> goodInfoImgList) {
        Long goodId = goodMapper.insertNewGood(good);
        if (!Objects.isNull(goodId)) {
            // 快速构建项目，不使用xml进行单次DB连接插入
            for (int i = 0; i< goodMainImgList.size(); i++) {
                GoodMainImg img = GoodMainImg.builder()
                        .gid(goodId)
                        .imgPath(goodMainImgList.get(i))
                        .state(Long.valueOf("1")).build();
                goodMainImgMapper.insert(img);
            }
            // 为了快速构建项目，不使用xml进行单次DB连接插入
            for (String infoImgPath : goodInfoImgList) {
                GoodIntroImg img = GoodIntroImg.builder()
                        .gid(goodId).imgPath(infoImgPath)
                        .state(Long.valueOf("1")).build();
                goodIntroImgMapper.insert(img);
            }
            // 为了快速构建项目，不使用xml进行单次DB连接插入
            for (GoodDetail goodDetail : goodDetailList) {
                goodDetail.setGoodId(goodId);
                goodDetailMapper.insert(goodDetail);
            }
            return 1;
        }
        return 0;
    }
}

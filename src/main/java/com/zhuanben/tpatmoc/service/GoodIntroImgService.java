package com.zhuanben.tpatmoc.service;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhuanben.tpatmoc.domain.GoodIntroImg;
import com.zhuanben.tpatmoc.domain.GoodMainImg;
import com.zhuanben.tpatmoc.mapper.GoodIntroImgMapper;
import com.zhuanben.tpatmoc.util.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class GoodIntroImgService {
    @Autowired
    GoodIntroImgMapper goodIntroImgMapper;

    /**
     * 获取全部主图
     * @param goodId
     * @return 包含全部主图Object的List
     */
    public List<GoodIntroImg> getAllGoodIntroImg(Long goodId){
        List<GoodIntroImg> goodIntroImgs = (List<GoodIntroImg>)RedisUtils.get("allGoodIntroImg_"+goodId, List.class);
        if(Objects.isNull(goodIntroImgs)) {
            QueryWrapper<GoodIntroImg> introImgQueryWrapper = new QueryWrapper<>();
            introImgQueryWrapper.eq("gid", goodId);
            goodIntroImgs = goodIntroImgMapper.selectList(introImgQueryWrapper);
            RedisUtils.set("allGoodIntroImg_"+goodId, goodIntroImgs, RedisUtils.ONE_MONTH);
        }
        return  goodIntroImgs;
    }
}

package com.zhuanben.tpatmoc.service;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.zhuanben.tpatmoc.customizeException.IllegalRequestException;
import com.zhuanben.tpatmoc.domain.ReceiveInfo;
import com.zhuanben.tpatmoc.mapper.ReceiveInfoMapper;
import com.zhuanben.tpatmoc.util.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class ReceiveInfoService {
    @Autowired
    ReceiveInfoMapper receiveInfoMapper;


    public List<ReceiveInfo> getAllReceiveInfoByUid(Long uid){
        List<ReceiveInfo> receiveInfoList = (List<ReceiveInfo>)
                RedisUtils.get("receiveList_"+uid, List.class);
        if(Objects.isNull(receiveInfoList)){
            QueryWrapper<ReceiveInfo> receiveInfoQueryWrapper = new QueryWrapper<>();
            receiveInfoQueryWrapper.eq("uid", uid);
            receiveInfoQueryWrapper.eq("state", 0); // 只选择有效的收件地址信息 0正常
            receiveInfoList = receiveInfoMapper.selectList(receiveInfoQueryWrapper);
            RedisUtils.set("receiveList_"+uid,receiveInfoList, RedisUtils.ONE_HOUR*2);
        }
        return receiveInfoList;
    }

    /**
     * 删除指定的地址
     * @param recId
     */
    public boolean delOneReceInfoByRecId(Long recId,Long uid) {
        ReceiveInfo rec = new ReceiveInfo();
        rec.setId(recId);
        // 收件地址1是停用
        rec.setState(1);
        QueryWrapper<ReceiveInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", recId);
        ReceiveInfo receiveInfo = receiveInfoMapper.selectOne(queryWrapper);
        if(Objects.isNull(receiveInfo)){
            throw new IllegalRequestException("收件地址服务的非法请求");
        }
        if(receiveInfoMapper.updateById(rec)>0){
            RedisUtils.delete("receiveList_"+uid);
            return true;
        }
        return false;

    }

    /**
     * 添加收件地址
     * @param receiveInfo
     * @return
     */
    public boolean insertNewReceiveInfo(ReceiveInfo receiveInfo) {
        receiveInfo.setState(0);
        if(receiveInfoMapper.insert(receiveInfo)>0){
            RedisUtils.delete("receiveList_"+receiveInfo.getUid());
            return true;
        }
        return false;
    }

    @Deprecated
    public boolean updateReceiveInfo(ReceiveInfo receiveInfo){
        try {
            receiveInfoMapper.updateById(receiveInfo);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 删除所有无效的收件地址
     * @return 不抛出异常则删除成功
     */
    @Deprecated
    public boolean deleteAllUnusefulReceiveInfo() {
        UpdateWrapper<ReceiveInfo> receiveInfoUpdateWrapper = new UpdateWrapper<>();
        receiveInfoUpdateWrapper.eq("state", "2");
        try {
            receiveInfoMapper.delete(receiveInfoUpdateWrapper);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

    }
}

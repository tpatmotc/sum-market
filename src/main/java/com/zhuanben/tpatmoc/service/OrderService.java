package com.zhuanben.tpatmoc.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhuanben.tpatmoc.config.RabbitMQConfig;
import com.zhuanben.tpatmoc.customizeException.IllegalRequestException;
import com.zhuanben.tpatmoc.customizeException.PurchaseException.PurchaseException;
import com.zhuanben.tpatmoc.customizeException.PurchaseException.StockNotEnoughException;
import com.zhuanben.tpatmoc.domain.GoodDetail;
import com.zhuanben.tpatmoc.domain.Order;
import com.zhuanben.tpatmoc.domain.OrderDetail;
import com.zhuanben.tpatmoc.domain.dto.OrderDTO;
import com.zhuanben.tpatmoc.mapper.*;
import com.zhuanben.tpatmoc.service.Impl.QueueMessageProducerServiceImpl;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class OrderService{
    @Autowired
    OrderMapper orderMapper;

    @Autowired
    GoodDetailMapper goodDetailMapper;

    @Resource
    OrderDetailMapper orderDetailMapper;

    @Autowired
    GoodDetailService goodDetailService;

    @Resource
    OrderDetailService orderDetailService;

    @Resource
    QueueMessageProducerServiceImpl orderRabbitMqService;

    @Resource
    CommentService commentService;

    /**
     * 下订单
     * @param orderdto
     * @return
     * @throws SQLException
     */
    @Transactional(rollbackFor = Exception.class)
    public Long createOneOrder(OrderDTO orderdto) throws SQLException {
//        // 先检查starPoint点数是否足够
//        QueryWrapper<SystemUser> userQueryWrapper = new QueryWrapper<>();
//        userQueryWrapper.select("star_point", "id").eq("id", orderdto.getOrderUid());
//        SystemUser systemUser = userMapper.selectOne(userQueryWrapper);
//        if(systemUser.getStarPoint() != orderdto.getStarPoint()){
//            throw new IllegalRequestException("OrderService starPoint的非法请求");
//        }
        // 计算出总价
//        BigDecimal totalPrice = new BigDecimal(orderdto.getOrderPayment())
//                .subtract(new BigDecimal(systemUser.getStarPoint()/100));
        Order order = Order.builder().createTime(orderdto.getOrderCreateTime())
                .payment(new BigDecimal(orderdto.getOrderPayment()))
                .receiveId(orderdto.getOrderReceiveId())
                .uid(orderdto.getOrderUid()).build();
        List<OrderDetail> orderDetailList = orderdto.getOrderDetailList();
        // 获取商品的skuid列表并转字符串，方便下文直接传入mapper去获取长度
        List<Long> skuidList = orderDetailList.stream().
            map(OrderDetail::getSkuid).collect(Collectors.toList());
//        System.out.println("skuidList--->"+skuidList);
        // 获取对应skuid的stock
        List<Integer> stockList = goodDetailMapper.getStockListBySkuidList(skuidList);
        if(stockList.size() != orderdto.getOrderDetailList().size()){
//            System.out.println(stockList.size()+"---"+orderdto.getOrderDetailList().size());
            throw new IllegalRequestException("OrderService的非法请求");
        }

        // 遍历orderDetail里面的每个商品的数量
        for(int index = 0;index < orderdto.getOrderDetailList().size(); index++){
//            System.out.println("line --90");
            // 比较购买数量是否小于等于库存
//            System.out.println(orderdto.getOrderDetailList().get(index).getAmount());
//            System.out.println(stockList.get(index));
            if(orderdto.getOrderDetailList().get(index).getAmount() > stockList.get(index)){
                order.setState(5);
                orderMapper.updateById(order);
                throw new StockNotEnoughException("商品号:"+orderdto.getOrderDetailList().get(index)
                        .getSkuid().toString()+"库存不足");
            }
        }
        // 库存大于等于购买数量-> 创建订单-> state = 1未付款
        order.setState(1);
        orderMapper.createOneOrder(order);
        // 创建订单完成，去创建OrderDetail
        if(!orderDetailService.createOrderDetail(orderdto.getOrderDetailList(), order.getId())){
            throw new PurchaseException("未知错误");
        }
        // 创建orderDetail也完成后，减小对应库存数量
        goodDetailService.steepStock(orderDetailList);
        //防止redis读脏数据，新增订单后删除有关订单的全部缓存
//        System.out.println("see see here---->"+orderdto.getOrderUid());
//        RedisUtils.delete("orderInfo_"+orderdto.getOrderUid()+"state_null");
//        RedisUtils.delete("orderInfo_"+orderdto.getOrderUid()+"state_1");
//        RedisUtils.delete("orderInfo_"+orderdto.getOrderUid()+"state_2");
//        RedisUtils.delete("orderInfo_"+orderdto.getOrderUid()+"state_3");
//        RedisUtils.delete("orderInfo_"+orderdto.getOrderUid()+"state_4");
//        RedisUtils.delete("orderInfo_"+orderdto.getOrderUid()+"state_5");
        // 扣除相对应的starPoint
//        UpdateWrapper<SystemUser> userUpdateWrapper = new UpdateWrapper<>();
//        userUpdateWrapper.setSql("update t_user set star_point = star_point - " + totalPrice.toString()+ "" +
//                "where t_user.id = " + orderdto.getOrderUid()+"");
//        userMapper.update(systemUser, userUpdateWrapper);

        try{
            // 往rabbitMQ写一个等待订单自动取消的数据
            Map map = new HashMap();
            map.put("orderId", order.getId());
//            System.out.println(map);
            orderRabbitMqService.sendOrderProcess(map, RabbitMQConfig.ROUTING_KEY_ORDER_CANCEL,
                    RabbitMQConfig.EXCHANGE_DLX);
        }catch (Exception e){
//            System.out.println("orderService..createOrder的rabbitMq抛出异常");
            e.printStackTrace();
        }


        return order.getId();

    }

    /**
     * 获取用户id下的订单概览列表
     * @author 余志豪
     * @param uid
     * @return 所有状态的所有订单 allOrderInfo
     */
    public List<OrderDTO> getAllOrderByUidAndState(Long uid, Integer state) {
        List<OrderDTO> allOrderInfo = null;
//                = (List<OrderDTO>)RedisUtils.get("orderInfo_"+uid+"state_"+state, List.class) ;
        if(Objects.isNull(allOrderInfo)){
            Map map = new HashMap();
            map.put("uid", uid);
            map.put("state", state);
            allOrderInfo = orderMapper.getOrderByUid(map);
        }
        return allOrderInfo;
    }

    /**
     * 确认付款用
     * @param orderDTO
     * @return
     * @throws Exception 这个exception是rabbitMq抛出的
     */
    public boolean confirmPay(OrderDTO orderDTO) throws Exception {
        Order order = Order.builder().payTime(orderDTO.getOrderPayTime())
                .id(orderDTO.getOrderId()).state(2).build();
        if(orderMapper.updateById(order)>0){
            Map map = new HashMap();
            map.put("orderId", orderDTO.getOrderId());
            orderRabbitMqService.sendOrderProcess(map, RabbitMQConfig.ROUTING_KEY_ORDER_AUTO_CONFIRM,
                    RabbitMQConfig.EXCHANGE_DLX_AUTO_CONFIRM);
            return true;
        }else{
            // 此处相当于检查了一次订单是否已经付款，修改失败说明是非法的请求
            throw new IllegalRequestException("非法的请求");
        }
    }

    /**
     * 退整个订单
     * @param orderID
     * @return
     */
    @Transactional(rollbackFor=IllegalRequestException.class)
    public boolean cancelOrderByOrderId(Long orderID) {

        Order order = orderMapper.selectById(orderID);
        if(order.getState() == 3){
            throw new IllegalRequestException("非法请求"); // 如果是rabbitMQ的内部调用，只是为了将其停止而抛出异常
        }
        List<OrderDetail> orderDetailList =
                orderDetailService.getOrderDetailByOrderId(orderID);
        List<GoodDetail> goodDetailList = new ArrayList<>();
        List<Long> orderDetailIdList = new ArrayList<>();
        for (OrderDetail od: orderDetailList){
            if(od.getState() == 1 || od.getState() == 2){
                System.out.println(od);
                goodDetailList.add(GoodDetail.builder()
                                    .skuid(od.getSkuid()).
                                    stock(od.getAmount()).build());
                orderDetailIdList.add(od.getId());
            }
        }
        // 退款后改变库存
        try {

//            System.out.println(goodDetailList.size());
            int i = goodDetailMapper.riseStockByGoodDetailList(goodDetailList);
            int j = orderDetailMapper.setUnusefulStateByDetailIdList(orderDetailIdList);
//            System.out.println(i);
            if(i > 0 && j > 0){
                Order o = Order.builder().id(orderID).state(3).finishTime(new Date()).build();
                orderMapper.updateById(o);
                return true;
                // todo 退款后把子订单也设为失效状态
            }else {
                throw new IllegalRequestException();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return false;
    }

    /**
     * 获取订单详细信息用（包含创建时间等的详细页面）
     * @param orderId
     * @return
     */
    public OrderDTO getOrderThoroughInfoByOrderId(Long orderId){
        OrderDTO thoroughOrderInfo = orderDetailMapper.getOrderThoroughInfoByOrderId(orderId);
        for (OrderDetail od: thoroughOrderInfo.getOrderDetailList()) {
            od.setIdStr(od.getId().toString());
            // 判断是否已经评价过该商品了
            od.setIsCommented(Boolean.valueOf(commentService.checkIsCommented(od.getId())));// 返回true表示已经评价过了
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String orderCreateTimeStr = null;
        String orderPayTimeStr = null;
        String orderFinishTimeStr = null;
        // 将时间后端格式化String后再发给前端，避免页面抖动。
        if(thoroughOrderInfo.getOrderCreateTime()!=null)
            orderCreateTimeStr = simpleDateFormat.format(thoroughOrderInfo.getOrderCreateTime());
        if (thoroughOrderInfo.getOrderPayTime() !=null)
            orderPayTimeStr= simpleDateFormat.format(thoroughOrderInfo.getOrderPayTime());
        if (thoroughOrderInfo.getOrderFinishTime()!=null)
            orderFinishTimeStr = simpleDateFormat.format(thoroughOrderInfo.getOrderFinishTime());

        thoroughOrderInfo.setCreateTimeString(orderCreateTimeStr);
        thoroughOrderInfo.setPayTimeString(orderPayTimeStr);
        thoroughOrderInfo.setFinishTimeString(orderFinishTimeStr);
        return thoroughOrderInfo;
    }

    /**
     * 通过传入的orderId查询数据库内该订单的状态，供rabbitMQ使用
     * @param orderId
     * @return
     */
    public Integer getOrderStateByOrderId(Long orderId){
        QueryWrapper<Order> orderQueryWrapper = new QueryWrapper<>();
        orderQueryWrapper.select("state");
        orderQueryWrapper.eq("id", orderId);
        Order order = orderMapper.selectOne(orderQueryWrapper);
        return order.getState();
    }

    /**
     * 确认收货使用
     * @param orderId
     * @return
     * @Exception exception 这个异常由rabbit抛出
     */
    public boolean confirmReceiveByOrderId(Long orderId,Long finishTime){
        Order tempOrder = orderMapper.selectById(orderId);
        if(tempOrder.getState() == 4){
            throw new IllegalRequestException("订单确认收货的非法请求");
        }
        Order o = Order.builder().id(orderId).finishTime(new Date(finishTime)).state(4).build();
        int i = orderMapper.updateById(o);
        if(i>0){
            //防止redis读脏数据，新增订单后删除有关订单的全部缓存
//            System.out.println("see see here---->"+tempOrder.getUid());
//            RedisUtils.delete("orderInfo_"+tempOrder.getUid()+"state_null");
//            RedisUtils.delete("orderInfo_"+tempOrder.getUid()+"state_1");
//            RedisUtils.delete("orderInfo_"+tempOrder.getUid()+"state_2");
//            RedisUtils.delete("orderInfo_"+tempOrder.getUid()+"state_3");
//            RedisUtils.delete("orderInfo_"+tempOrder.getUid()+"state_4");
//            RedisUtils.delete("orderInfo_"+tempOrder.getUid()+"state_5");
            return true;
        }else {
            throw new IllegalRequestException("订单确认收货的非法请求");
        }
    }
}

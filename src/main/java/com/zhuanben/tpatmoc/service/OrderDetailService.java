package com.zhuanben.tpatmoc.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.zhuanben.tpatmoc.customizeException.IllegalRequestException;
import com.zhuanben.tpatmoc.domain.GoodDetail;
import com.zhuanben.tpatmoc.domain.Order;
import com.zhuanben.tpatmoc.domain.OrderDetail;
import com.zhuanben.tpatmoc.domain.po.OrderDetailPO;
import com.zhuanben.tpatmoc.mapper.GoodDetailMapper;
import com.zhuanben.tpatmoc.mapper.OrderDetailMapper;
import com.zhuanben.tpatmoc.mapper.OrderMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderDetailService {
    @Resource
    OrderDetailMapper orderDetailMapper;

    @Resource
    GoodDetailMapper goodDetailMapper;

    @Resource
    OrderMapper orderMapper;


    public boolean createOrderDetail(List<OrderDetail> list,Long orderId){
        OrderDetailPO orderDetailPO = new OrderDetailPO();
        orderDetailPO.setList(list);
        orderDetailPO.setOrderId(orderId);
//        System.out.println(orderDetailPO);
        try {
            orderDetailMapper.insertOrderDetailByOrderDetailPO(orderDetailPO);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }


    /**
     * 用orderId获取数该id下全部orderDetail
     * 不用于获取细节详情发送到前端
     * @param orderId
     * @return
     */
    public List<OrderDetail> getOrderDetailByOrderId(Long orderId) {
        QueryWrapper<OrderDetail> orderDetailQueryWrapper = new QueryWrapper<>();
        orderDetailQueryWrapper.eq("oid", orderId);
        List<OrderDetail> orderDetailList = orderDetailMapper.selectList(orderDetailQueryWrapper);
        return orderDetailList;
    }

    /**
     * 用于退款订单内的子订单
     */
    @Transactional(rollbackFor= Exception.class)
    public boolean cancelOrderDetailByOrderDetailId(Long orderDetailId){
        // TODO 需要先判断状态
        OrderDetail orderDetail = orderDetailMapper.selectById(orderDetailId);
        if(orderDetail.getState() == 3){
            throw new IllegalRequestException("cancelOrderDetailByOrderDetailId-> exception 1");
        }
//        System.out.println(orderDetail);
        List<GoodDetail> goodDetailList = new ArrayList<>();
        goodDetailList.add(GoodDetail.builder().skuid(orderDetail.getSkuid()).
                stock(orderDetail.getAmount()).build());
        try {
            // 退款后释放库存
            int i = goodDetailMapper.riseStockByGoodDetailList(goodDetailList);
            if(i == 1){
                // 添加库存成功后改变子订单状态
                orderDetail.setState(Long.valueOf("3"));
                orderDetailMapper.updateById(orderDetail);

                // 检测这个子订单的父级订单是否存在其他订单，若其他订单也已经退款，把整个订单设为3（失效）
                QueryWrapper<OrderDetail> orderDetailQueryWrapper = new QueryWrapper<>();
                orderDetailQueryWrapper.eq("oid", orderDetail.getOid());
                List<OrderDetail> orderDetailList = orderDetailMapper.selectList(orderDetailQueryWrapper);

                boolean isAllOrderUseless = true; // 判断整个订单是否已经无效的标志位
                for (OrderDetail detail : orderDetailList) {
                    if(detail.getState() != 3){ // 至少有一个子订单还有效
                        isAllOrderUseless = false;
                        break;
                    }
                }
                // 退款子订单后，修改订单实际付款
                Order order = orderMapper.selectById(orderDetail.getOid());
                order.setPayment(order.getPayment().subtract(orderDetail.getUnitPayment()));
                orderMapper.updateById(order);


                // 判断整个订单是否已经无效，无效则把父级订单也置为3（失效）
                if(isAllOrderUseless){
                    order = Order.builder().id(orderDetail.getOid()).state(3).build();
                    orderMapper.updateById(order);
                }

                return true;
            }else {
                throw new IllegalRequestException();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

}

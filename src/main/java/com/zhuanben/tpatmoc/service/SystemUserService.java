package com.zhuanben.tpatmoc.service;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhuanben.tpatmoc.customizeException.WechatException.WechatException;
import com.zhuanben.tpatmoc.domain.LoginUser;
import com.zhuanben.tpatmoc.domain.SystemUser;
import com.zhuanben.tpatmoc.domain.dto.SystemUserDTO;
import com.zhuanben.tpatmoc.mapper.UserMapper;
import com.zhuanben.tpatmoc.service.Impl.UserDetailsServiceImpl;
import com.zhuanben.tpatmoc.util.DateUtils;
import com.zhuanben.tpatmoc.util.JwtUtils;
import com.zhuanben.tpatmoc.util.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.*;

@Service
public class SystemUserService {
    @Resource
    UserMapper userMapper;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserDetailsServiceImpl userDetailsServiceImpl;


    @Resource
    PasswordEncoder passwordEncoder;

    @Value("${WECHAT_APPID}")
    private String APPID ;

    @Value("${WECHAT_SECRET}")
    private String SECRET;



    /**
     * @param user controller接收 传入的实体类SystemUser对象
     * @return int-> 1:用户名已存在，2->微信号已注册，3->电话号码已注册，4->注册成功 5->内部错误
     * @author 余志豪
     */
    public int register(SystemUser user) {
        QueryWrapper<SystemUser> userWrapper = new QueryWrapper();
        userWrapper.eq("username", user.getUsername());
        try {
            if (userMapper.selectOne(userWrapper) != null) {
                return 1;
            }
//            userWrapper = null;
//            userWrapper = new QueryWrapper<>();
//            userWrapper.eq("wechat_id", user.getWechatId());
//            if (userMapper.selectOne(userWrapper) != null) {
//                return 2;
//            }
//            userWrapper = null;
            userWrapper = new QueryWrapper<>();
            userWrapper.eq("tel", user.getTel());
            if (userMapper.selectOne(userWrapper) != null) {
                return 3;
            }
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            if (userMapper.insert(user) > 0) {
                return 4;
            }
        } catch (Exception e) {
            return 5;
        }
        return 5;
    }

    public Map login(SystemUser systemUser) {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(systemUser.getUsername(), systemUser.getPassword());
        // 默认调用UserDetailsServiceImpl类中的loadUserByUsername获取用户信息并进行认证，
        // 再把用户信息封装在authenticate对象中
//        System.out.println(authenticationToken);
        Authentication authenticate = authenticationManager.authenticate(authenticationToken);
        if(Objects.isNull(authenticate)){
            throw new BadCredentialsException("用户名或密码错误");
        }
        LoginUser loginUser = (LoginUser) authenticate.getPrincipal();//使用SystemUser.uid生成token
//        System.out.println(loginUser.getSystemUser().getId());
        Map<String, String> response = JwtUtils.createDoubleToken(loginUser.getSystemUser().getId().toString());
        RedisUtils.set
                ("login_"+loginUser.getSystemUser().getId(),loginUser,RedisUtils.ONE_HOUR);// 1小时的redis token存储
        response.put("userId", loginUser.getSystemUser().getId().toString());
        return response;
    }

    public SystemUser getSystemUserByUid(Long id){
        SystemUser user = (SystemUser) RedisUtils.get("user_"+id, SystemUser.class);
        if(Objects.isNull(user)){
            user = userMapper.selectById(id);
            RedisUtils.set("user_" + id, user, RedisUtils.ONE_WEEK);
        }
        return user;
    }

    /**
     *
     * @param jsCode uni-app前端传入的js_code，用于获取用户openid等数据
     * @return
     */
    public Map<String, String> getWechatUserInfo(String jsCode){
        String url = "https://api.weixin.qq.com/sns/jscode2session" +
                "?appid="+APPID+
                "&secret="+SECRET+
                "&js_code="+jsCode+
                "&grant_type=authorization_code";
        MultiValueMap<String, String> requestEntity = new LinkedMultiValueMap<>();
        Map paraMap = new HashMap();
        paraMap.put("type", "wx");
        paraMap.put("mchid", "10101");
        requestEntity.add("consumerAppId", "test");
        requestEntity.add("serviceName", "queryMerchant");
        requestEntity.add("params", JSON.toJSONString(paraMap));
        RestTemplate restTemplate = new RestTemplate();
        String userJson = restTemplate.getForObject(url, String.class, requestEntity);
        Map userInfoMap = JSON.parseObject(userJson, Map.class);



        if(!Objects.isNull(userInfoMap.get("errcode"))){
            throw new RuntimeException(userInfoMap.get("errmsg").toString());
        }
        return userInfoMap;
    }


    public Map  wechatService(SystemUserDTO systemUserDTO){
        Map<String, String> wechatUserInfo = getWechatUserInfo(systemUserDTO.getJsCode());
        String openid = wechatUserInfo.get("openid");

        // 查看redis有没有这个systemUser数据
        SystemUser systemUser = (SystemUser) RedisUtils.get("user_" + openid, SystemUser.class);
        // redis没有就去SQL查询
        if(Objects.isNull(systemUser)){
            systemUser = userDetailsServiceImpl.getSystemUserByOpenId(openid);

        }
        // 如果可以得到这个systemUser说明此openid这个存在，不往下执行if的代码
        if(Objects.isNull(systemUser)) {
            systemUser = systemUserDTO.getSystemUser();
            systemUser.setStarPoint(0);
            // username的内容为空,则给他生成一个默认的 --->
            if (Objects.isNull(systemUser.getUsername())) {
                String tempUsername = "user_" + JwtUtils.getUUID();
                systemUser.setUsername(tempUsername);
            }
            // username 处理完毕 <===


            // 设置密码为空则自动生成密码 --->
            if (Objects.isNull(systemUser.getPassword())) {
                String tempPwd = JwtUtils.getUUID().substring(0, 6);
                tempPwd = passwordEncoder.encode(tempPwd);
                systemUser.setPassword(tempPwd);
            } else {
                systemUser.setPassword(passwordEncoder.encode(systemUser.getPassword()));
            }
            // 密码处理完毕  <===

            // 设置头像跟昵称(默认值)
            if(Objects.isNull(systemUser.getNickname())){
                systemUser.setNickname("用户_"+new Date().getTime());
            }
            if (Objects.isNull(systemUser.getAvatar())){
                systemUser.setAvatar("https://thirdwx.qlogo.cn/mmopen/vi_32/POgEwh4mIHO4nibH0KlMECNjjGxQUq24ZEaGT4poC6icRiccVGKSyXwibcPq4BWmiaIGuG1icwxaQX6grC9VemZoJ8rg/132");systemUser.setAvatar("https://thirdwx.qlogo.cn/mmopen/vi_32/POgEwh4mIHO4nibH0KlMECNjjGxQUq24ZEaGT4poC6icRiccVGKSyXwibcPq4BWmiaIGuG1icwxaQX6grC9VemZoJ8rg/132");
            }

            systemUser.setOpenId(openid);
            // 写入数据库
            try{
                userMapper.insert(systemUser);
            }catch (Exception e){
                e.printStackTrace();
                // 写入数据库失败，可能是非法请求，不予响应。
                throw new WechatException(e.getMessage());
            }
        }

        RedisUtils.set("user_"+systemUser.getId(), systemUser, RedisUtils.ONE_DAY);

        UserDetails userDetails = userDetailsServiceImpl.loadUserByUsername(systemUser.getUsername());
        LoginUser loginUser2 = (LoginUser) userDetails;
        RedisUtils.set("login_"+loginUser2.getSystemUser().getId(),loginUser2, JwtUtils.ONE_HOUR);
//        System.out.println(loginUser2.getSystemUser().getId());

        Map<String, String> response = JwtUtils.createDoubleToken(loginUser2.getSystemUser().getId().toString());
        response.put("userId", loginUser2.getSystemUser().getId().toString());
        response.put("star_point", systemUser.getStarPoint().toString());
        return response;
    }

    @Deprecated
    public Map loginWechat(String code) {
        Map<String, String> wechatUserInfo = getWechatUserInfo(code);
        String openid = wechatUserInfo.get("openid");
        SystemUser systemUser = userDetailsServiceImpl.getSystemUserByOpenId(openid);
        if(!Objects.isNull(systemUser)){
            Map<String, String> response = new HashMap<>();
            String access_token = JwtUtils.createJWT(systemUser.getId().toString(), JwtUtils.ONE_HOUR);
            String refresh_token = JwtUtils.createJWT(systemUser.getId().toString(), JwtUtils.ONE_WEEK);
            response.put("access_token", access_token);
            response.put("refresh_token", refresh_token);

            response.put("msg", "登陆成功");
            return response;
        }
        return null;
    }

    @Deprecated
    public boolean registerWechat(SystemUserDTO systemUserDTO) {
        Map<String, String> wechatUserInfo = getWechatUserInfo(systemUserDTO.getJsCode());
        SystemUser user = systemUserDTO.getSystemUser();
        // 判断用户名是否空
        if(Objects.isNull(user.getUsername())){
            String username = JwtUtils.getUUID();
            username = "user_" + username.substring(0,6);
            user.setUsername(username);
        }
        // 判断密码是否空
        if (Objects.isNull(user.getPassword())) {
            String pwd = JwtUtils.getUUID();
            pwd =  pwd.substring(0,6);
            user.setPassword( passwordEncoder.encode(pwd));
        } else{
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }

        user.setOpenId(wechatUserInfo.get("openid"));
        try{
            userMapper.insert(user);
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean signIn(Long uid) {
        String s = RedisUtils.get("signIn:user_" + uid);
        if(Objects.isNull(s)){
            if (userMapper.signIn(uid)>0){
                RedisUtils.set("signIn:user_" + uid, uid, DateUtils.getTodayRemainSecondNum());
                return true;
            }
        }

        return  false;
    }

    public SystemUserDTO getStarPointByUid(Long Uid){
        QueryWrapper<SystemUser> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.select("star_point");
        userQueryWrapper.eq("id", Uid);
        SystemUser systemUser = userMapper.selectOne(userQueryWrapper);
        SystemUserDTO systemUserDTO = SystemUserDTO.builder().systemUser(systemUser).build();
        // 从redis中读取签到数据，如果今天没有签到过(redis没有数据)，则设为false
        if(Objects.isNull(RedisUtils.get("signIn::" + Uid))){
            systemUserDTO.setSignToday(false);
        }else {
            systemUserDTO.setSignToday(true);
        }
        return systemUserDTO;

    }
}

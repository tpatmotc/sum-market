package com.zhuanben.tpatmoc.service;


import com.zhuanben.tpatmoc.domain.Role;
import com.zhuanben.tpatmoc.domain.SystemUser;
import com.zhuanben.tpatmoc.mapper.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class RoleService {
    @Autowired
    RoleMapper roleMapper;

    public List<Role> getRoleByUid(String systemUserName) {
        return roleMapper.getRoleByUser(systemUserName);
    }
}

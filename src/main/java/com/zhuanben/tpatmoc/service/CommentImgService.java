package com.zhuanben.tpatmoc.service;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhuanben.tpatmoc.domain.CommentImg;
import com.zhuanben.tpatmoc.mapper.CommentImgMapper;
import com.zhuanben.tpatmoc.util.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CommentImgService {
    @Autowired
    CommentImgMapper commentImgMapper;

        public List<String> getCommentImgsByCommentId(Long commentId) {
        List<String> commPathList = JSON.parseArray(RedisUtils.get("commImgs_"+commentId), String.class);
        if(Objects.isNull(commPathList)){
            QueryWrapper<CommentImg> imgQueryWrapper = new QueryWrapper<>();
            imgQueryWrapper.eq("comm_id", commentId);
             commPathList = commentImgMapper.selectList(imgQueryWrapper).
                            stream().map(CommentImg::getImgPath).collect(Collectors.toList());
             RedisUtils.set("commImgs_"+commentId, commPathList,RedisUtils.ONE_WEEK);
        }
        return commPathList;
    }
}

package com.zhuanben.tpatmoc.service;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhuanben.tpatmoc.domain.Comment;
import com.zhuanben.tpatmoc.domain.CommentImg;
import com.zhuanben.tpatmoc.domain.GoodDetail;
import com.zhuanben.tpatmoc.domain.SystemUser;
import com.zhuanben.tpatmoc.domain.dto.CommentDTO;
import com.zhuanben.tpatmoc.domain.po.CommImgPO;
import com.zhuanben.tpatmoc.mapper.CommentImgMapper;
import com.zhuanben.tpatmoc.mapper.CommentMapper;
import com.zhuanben.tpatmoc.mapper.GoodDetailMapper;
import com.zhuanben.tpatmoc.util.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CommentService {
    @Autowired
    CommentMapper commentMapper;

    @Autowired
    CommentImgMapper commentImgMapper;

    @Resource
    GoodDetailMapper goodDetailMapper;

    @Resource
    SystemUserService systemUserService;

    @Resource
    CommentImgService commentImgService;

    /**
     *
     * @param goodId 传入的商品详情号
     * @param page
     * @return
     */

    public List<CommentDTO> getCommentsByGoodId(Long goodId, Integer page) {
        String redisKey = "comment_"+goodId.toString()+"_page_"+page.toString();
        List<Comment> commentRecords = JSON.parseArray(RedisUtils.get(redisKey), Comment.class);
//        System.out.println(commentRecords);
        if(Objects.isNull(commentRecords)){
            // 先查看这个goodId下有哪些个skuid
            QueryWrapper<GoodDetail> goodDetailQueryWrapper = new QueryWrapper<>();
            goodDetailQueryWrapper.select("skuid");
            goodDetailQueryWrapper.eq("good_id",goodId);
            List<Long> skuidList = goodDetailMapper.selectList(goodDetailQueryWrapper)
                    .stream().map(GoodDetail::getSkuid).collect(Collectors.toList());

            QueryWrapper<Comment> commentQueryWrapper = new QueryWrapper<>();
            commentQueryWrapper.select("comm_level","uid","content","publish_time","id");

            // 限制只获取可见的评论
            commentQueryWrapper.eq("state", 1);
            commentQueryWrapper.in("skuid", skuidList);
            // param_1-> page：页数，param_2-> 10每页几条数据
            Page<Comment> commentPage = new Page<>(page, 8);
            Page<Comment> comments = commentMapper.selectPage(commentPage, commentQueryWrapper);
            // 获取Page里面的一个个comment
            commentRecords = comments.getRecords();
            RedisUtils.set(redisKey, commentRecords, RedisUtils.ONE_MINUTE);
        }
//        System.out.println(commentRecords);
        // 构造真正的包含评论图片的评论
        List<CommentDTO> commentDTOList = new ArrayList<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String time ;
        CommentDTO commentDTO;

        for (Comment c: commentRecords) {
            time = simpleDateFormat.format(c.getPublishTime());
            SystemUser user = systemUserService.getSystemUserByUid(c.getUid());
//            System.out.println("->>>>>>"+user);
            commentDTO = CommentDTO.builder()
                    .publishTime(time).commLevel(c.getCommLevel()).content(c.getContent())
                    .avatar(user.getAvatar()).nickname(user.getNickname())
                    .commentImgList(commentImgService.getCommentImgsByCommentId(c.getId())).build();
            commentDTOList.add(commentDTO);
        }
//        System.out.println(commentDTOList);
        return commentDTOList;
    }


    public boolean createOneComment(CommentDTO commentDTO) {
        Comment comment = Comment.builder()
                .uid(commentDTO.getUid()).content(commentDTO.getContent())
                .orderDetailId(Long.valueOf(commentDTO.getOrderDetailId()))
                .publishTime(new Date(Long.valueOf(commentDTO.getPublishTime())))
                .commLevel(commentDTO.getCommLevel()).skuid(commentDTO.getSkuid())
                .build();
        if(commentMapper.insertComment(comment) > 0){
//            System.out.println(comment);
            // 判断评论是否有上传图片
            if(commentDTO.getCommentImgList().size()>0) {
                // 如果只有一张图片，直接Mybatis-Plus快速插入 --->
                if (commentDTO.getCommentImgList().size() == 1) {
                    CommentImg tempCommImg = CommentImg.builder().commId(comment.getId())
                            .imgPath(commentDTO.getCommentImgList().get(0)).build();
                    commentImgMapper.insert(tempCommImg);
                    // 如果有多张图片，用xml方式插入
                }else {
                    CommImgPO commImgPO = CommImgPO.builder().commId(comment.getId())
                            .commImgPathList(commentDTO.getCommentImgList()).build();
                    commentImgMapper.insertCommImgList(commImgPO);
                }
            }
            return true;
        }
        return false;
    }

    /**
     * 判断这个商品是否已经被评价过了
     * @param orderDetailId
     * @return
     */
    public boolean checkIsCommented(Long orderDetailId){
        Comment comment = (Comment) RedisUtils.get("comm_odid_"+orderDetailId, Comment.class);
        if( Objects.isNull(comment) ){
            QueryWrapper<Comment> commentQueryWrapper = new QueryWrapper<>();
            commentQueryWrapper.eq("order_detail_id",orderDetailId);
            comment = commentMapper.selectOne(commentQueryWrapper);
            RedisUtils.set("comm_odid_"+orderDetailId, comment);
        }
        // 返回值代表这个orderDetail指向的商品已经被评价过了是 true || false
        if(Objects.isNull(comment)){
            return false; // 没有被评价过
        }else {
            return true;  // 已经评价过了
        }



    }

    public Comment getSelfComment(Long orderDetailId) {
        Comment comment = (Comment) RedisUtils.get("comm_odid_"+orderDetailId, Comment.class);
        if( Objects.isNull(comment) ){
            comment = commentMapper.selectById(orderDetailId);
            RedisUtils.set("comm_odid_"+orderDetailId, comment);
        }
        return comment;
    }
}

package com.zhuanben.tpatmoc.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.zhuanben.tpatmoc.domain.GoodDetail;
import com.zhuanben.tpatmoc.domain.OrderDetail;
import com.zhuanben.tpatmoc.mapper.GoodDetailMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class GoodDetailService {
    @Autowired
    GoodDetailMapper goodDetailMapper;

    @Autowired
    GoodMainImgService goodMainImgService;

    @Autowired
    GoodIntroImgService goodIntroImgService;

    /**
     * 获取Good详细信息
     * @param goodId
     * @return goodDetails 是包含该goodId下的全部detail的列表
     */
    @Deprecated
    public List<GoodDetail> getGoodDetailByGoodId(Long goodId){
        QueryWrapper<GoodDetail> goodDetailQueryWrapper = new QueryWrapper<>();
        goodDetailQueryWrapper.eq("good_id", goodId);
        List<GoodDetail> goodDetails = goodDetailMapper.selectList(goodDetailQueryWrapper);
        return goodDetails;
    }

    @Deprecated
    public Integer getStockBySkuid(Long skuid) {
        QueryWrapper<GoodDetail> goodDetailQueryWrapper = new QueryWrapper<>();
        goodDetailQueryWrapper.select("stock");
        goodDetailQueryWrapper.eq("skuid",skuid);
        GoodDetail goodDetail = goodDetailMapper.selectOne(goodDetailQueryWrapper);
        return goodDetail.getStock();
    }

    /**
     * 减小库存数量的方法
     * @param orderDetailList
     */
    public boolean steepStock(List<OrderDetail> orderDetailList) throws SQLException {
//        System.out.println(orderDetailList);
        if (goodDetailMapper.steepStockByOrderDetailList(orderDetailList)>0){
            return true;
        }
        return false;
    }


}

package com.zhuanben.tpatmoc.controller;


import com.zhuanben.tpatmoc.customizeException.IllegalRequestException;
import com.zhuanben.tpatmoc.domain.ApiResult;
import com.zhuanben.tpatmoc.domain.ReceiveInfo;
import com.zhuanben.tpatmoc.service.ReceiveInfoService;
import com.zhuanben.tpatmoc.util.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@Api(tags="送货信息")
@RestController
@RequestMapping("/receive")
public class ReceiveInfoController {

    /*
        state 1是停止使用，0是正常
     */


    @Resource
    ReceiveInfoService receiveInfoService;

    @ApiOperation(value="查看收货信息",response = ReceiveInfo.class)
    @GetMapping("/receiveinfo/{uid}")
    @PreAuthorize("hasRole('ROLE_normal')")
    public ApiResult getAllReceiveInfoByUid(@PathVariable("uid") Long uid){
        // 验证是否是正常的请求
        if (!JwtUtils.verifyTokenAndUidIsSame(uid)) {
            throw new IllegalRequestException("非法请求");
        }

        List<ReceiveInfo> receiveInfoList = receiveInfoService.getAllReceiveInfoByUid(uid);
        return ApiResult.success(receiveInfoList);
    }

    @ApiOperation(value = "删除送货信息",response = ReceiveInfo.class)
    @DeleteMapping("/receiveinfo/{receiveInfoId}/{uid}")
    @PreAuthorize("hasAnyRole('ROLE_normal')")
    public ApiResult deleteOneReceiveInfo(@PathVariable("receiveInfoId")Long recId, @PathVariable("uid")Long uid){
        // 判断请求的用户和请求的用户uid是否是相同的 --->
        if(!JwtUtils.verifyTokenAndUidIsSame(uid)){
            throw new IllegalRequestException();
        }
        // 判断结束 <---

        if(receiveInfoService.delOneReceInfoByRecId(recId,uid)) {
            return ApiResult.success("删除成功");
        }
        return ApiResult.errorMsg("内部错误", ApiResult.SERVER_ERROR);
    }

    /**
     * uid也需要填
     * @param receiveInfo
     * @return
     */
    @ApiOperation(value = "添加收货地址")
    @PostMapping("/receiveinfo")
    @PreAuthorize("hasAnyRole('ROLE_normal')")
    public ApiResult insertNewReceiveInfo(@RequestBody ReceiveInfo receiveInfo){
        // 判断请求的用户和请求的用户uid是否是相同的 --->
        if(!JwtUtils.verifyTokenAndUidIsSame(receiveInfo.getUid())){
            throw new IllegalRequestException();
        }
        // 判断结束 <---

        receiveInfo.setState(0);
        if(receiveInfoService.insertNewReceiveInfo(receiveInfo)){
            return ApiResult.success("添加成功");
        }
        return ApiResult.errorMsg("添加失败");
    }

    @ApiOperation(value = "修改收货详情（禁止调用，存在修改异常）")
    @PutMapping("/receivceinfo")
    @PreAuthorize("hasAnyRole('ROLE_normal')")
    @Deprecated
    public ApiResult updateReceiveInfoById(@RequestBody ReceiveInfo receiveInfo){
        // 判断请求的用户和请求的用户uid是否是相同的 --->
        if(!JwtUtils.verifyTokenAndUidIsSame(receiveInfo.getUid())){
            throw new IllegalRequestException();
        }
        // 判断结束 <---

        if (receiveInfoService.updateReceiveInfo(receiveInfo)) {
            return ApiResult.success("修改成功");
        }
        return ApiResult.errorMsg("修改失败");
    }


    @Deprecated
    @ApiOperation(value = "清除无效收获地址(废弃，不允许调用！)")
    @DeleteMapping("/receiveinfo")
    @PreAuthorize("hasRole('ROLE_admin')")
    public ApiResult deleteUnusefulReceiveInfo(){
        if (receiveInfoService.deleteAllUnusefulReceiveInfo()) {
            return ApiResult.success("清理完成");
        }else {
            return ApiResult.errorMsg("清理失败,内部服务器错误",ApiResult.SERVER_ERROR);
        }
    }
}

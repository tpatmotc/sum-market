package com.zhuanben.tpatmoc.controller;

import com.zhuanben.tpatmoc.customizeException.IllegalRequestException;
import com.zhuanben.tpatmoc.domain.ApiResult;
import com.zhuanben.tpatmoc.domain.Comment;
import com.zhuanben.tpatmoc.domain.SystemUser;
import com.zhuanben.tpatmoc.domain.dto.CommentDTO;
import com.zhuanben.tpatmoc.service.CommentImgService;
import com.zhuanben.tpatmoc.service.CommentService;
import com.zhuanben.tpatmoc.service.SystemUserService;
import com.zhuanben.tpatmoc.util.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Api(tags="商品评论")
@RestController
@RequestMapping("/comment")
public class CommentController {
    @Autowired
    CommentService commentService;

    @Autowired
    CommentImgService commentImgService;

    @Resource
    SystemUserService systemUserService;


    /**
     *
     */
    @ApiOperation(value="获取评论信息和分页",response = CommentDTO.class)
    @GetMapping("/comments/{goodId}/{page}")
    @PreAuthorize("permitAll()")
    public ApiResult getCommentsByGoodIdAndPage(
            @PathVariable("goodId")Long goodId, @PathVariable("page") Integer page){
        // 获取comment的列表
        // todo 应该是通过goodId去获取评论，而不是skuid
        List<CommentDTO> commentList = commentService.getCommentsByGoodId(goodId, page);
        return ApiResult.success(commentList);
    }

    /**
     * @Param commentDTO ->
     */
    @ApiOperation(value = "根据goodId新增一条评论")
    @PostMapping("/comments")
    @PreAuthorize("hasRole('ROLE_normal')")
    public ApiResult createOneComment(@RequestBody CommentDTO commentDTO){
        // 判断请求的用户和请求的用户uid是否是相同的 --->
        if(!JwtUtils.verifyTokenAndUidIsSame(commentDTO.getUid())){
            throw new IllegalRequestException();
        }
        // 判断结束 <---
//        System.out.println(commentDTO);
        if( commentService.createOneComment(commentDTO)){

            return ApiResult.success("发送成功");
        }
        return ApiResult.errorMsg("发送失败");
    }

    @ApiOperation(value = "用户可以查看自己对该商品的评价")
    @GetMapping("/comment/{uid}/{orderDetailId}")
    @PreAuthorize("hasRole('ROLE_normal')")
    public ApiResult getSelfCommentByOrderDetailId(@PathVariable("uid")Long uid,
                                                   @PathVariable("orderDetailId")String orderDetailId){
        // 判断请求的用户和请求的用户uid是否是相同的 --->
        if(!JwtUtils.verifyTokenAndUidIsSame(uid)){
            throw new IllegalRequestException();
        }

        Comment selfComment = commentService.getSelfComment(Long.valueOf(orderDetailId));
        return ApiResult.success(selfComment);

    }


}

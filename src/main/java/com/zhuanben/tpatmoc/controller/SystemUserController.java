package com.zhuanben.tpatmoc.controller;

import com.zhuanben.tpatmoc.customizeException.IllegalRequestException;
import com.zhuanben.tpatmoc.customizeException.WechatException.WechatException;
import com.zhuanben.tpatmoc.domain.ApiResult;
import com.zhuanben.tpatmoc.domain.SystemUser;
import com.zhuanben.tpatmoc.domain.dto.SystemUserDTO;
import com.zhuanben.tpatmoc.service.SystemUserService;
import com.zhuanben.tpatmoc.util.JwtUtils;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Api(tags="用户")
@RestController
@RequestMapping("/user")
public class SystemUserController {
    @Autowired
    SystemUserService systemUserService;

    @Autowired
    RestTemplate restTemplate;

    @PostMapping("/register")
    @ApiOperation(value = "添加新用户")
    @ApiResponses({
            @ApiResponse(code = 200, message = "注册成功，即将返回登录页"),
            @ApiResponse(code = 500, message = "未知错误，请联系管理员"),
            @ApiResponse(code = 601, message = "用户名已存在"),
            @ApiResponse(code = 602, message = "该微信号已被注册"),
            @ApiResponse(code = 603, message = "该电话号码已被使用")
    })
    public ApiResult register(@ApiParam(required = true, value = "注册用户的基本信息")
                              @RequestBody SystemUser systemUser) {
        //    int-> 1:用户名已存在，2->微信号已注册，3->电话号码已注册，4->注册成功 ,其他->内部错误
        switch (systemUserService.register(systemUser)) {
            case 1:
                return ApiResult.errorMsg("用户名已存在");
            case 2:
                return ApiResult.errorMsg("该微信号已被注册");
            case 3:
                return ApiResult.errorMsg("该电话号码已被使用");
            case 4:
                return ApiResult.success("注册成功，即将返回登录页");
            default:
                return ApiResult.errorMsg("未知错误，请联系管理员");
        }
    }

    @PostMapping("/login")
    @ApiOperation(value = "用户登录")
    @ApiResponses({
            @ApiResponse(code = 200, message = "登陆成功", response = ResponseEntity.class, reference = "",
                    responseHeaders = {
                            @ResponseHeader(name = "token", response = String.class)
                    }),
            @ApiResponse(code = 401, message = "用户名或密码错误", response = ResponseEntity.class, reference = ""),
    })
    public ApiResult loginUser(@ApiParam(required = true, value = "登录用户的用户名和密码")
                                    @RequestBody SystemUser systemUser) {
        Map response = systemUserService.login(systemUser);
        if(response!=null){
            return ApiResult.success(response);
        }
        return ApiResult.errorMsg("用户名或密码错误");
    }

    @Deprecated
    @GetMapping("/wechat/login/{jscode}")
    @ApiOperation(value = "微信登录",response = SystemUserDTO.class)
    public ApiResult loginWechat(@PathVariable("jscode") String jsCode) {
//        System.out.println(jsCode);
        Map response = null;
        response = systemUserService.loginWechat(jsCode);
//        System.out.println(response);
        if(response != null){
            return ApiResult.success(response);
        }else {
            return ApiResult.errorMsg("登录失败");
        }


    }

    @Deprecated
    @PostMapping("/wechat/register")
    @ApiOperation(value = "微信注册")
    public ApiResult registerWechat(@RequestBody SystemUserDTO systemUserDTO){
//        System.out.println(systemUserDTO);
        if(systemUserService.registerWechat(systemUserDTO)){
            return ApiResult.success("注册成功");
        }else {
            return ApiResult.errorMsg("注册失败");
        }
    }

    @PostMapping("/wechat")
    @ApiOperation(value = "微信登录使用")
    public ApiResult wechat(@RequestBody SystemUserDTO systemUserDTO){
//        System.out.println(systemUserDTO);
        try {
            Map response = systemUserService.wechatService(systemUserDTO);
//            System.out.println(response);
            if(response == null){
                return ApiResult.errorMsg("该Username用户名已经被注册",ApiResult.CLIENT_ERROR);
                }
            return ApiResult.success(response);
        }catch (WechatException e){
            throw e;
        }
    }

    @GetMapping("/singIn/{uid}")
    @PreAuthorize("hasAnyRole('ROLE_normal')")
    @ApiModelProperty("签到获得5积分，需要token")
    public ApiResult signIn(@PathVariable("uid") Long uid){
//        System.out.println("调用signIn");
        if (!JwtUtils.verifyTokenAndUidIsSame(uid)) {
            throw new IllegalRequestException("非法请求");
        }
        if (systemUserService.signIn(uid)) {
            return ApiResult.success("签到成功");
        }
        return ApiResult.errorMsg("签到失败，当前已经签到过了");
    }

    @GetMapping("/starPoint/{uid}")
    @PreAuthorize("hasAnyRole('ROLE_normal')")
    @ApiOperation("获得star_point的当前点数以及当前的签到状态")
    public ApiResult getStarPoint(@PathVariable("uid") Long uid){
//        System.out.println("调用getStarPoint");
        if (!JwtUtils.verifyTokenAndUidIsSame(uid)) {
            throw new IllegalRequestException("非法请求");
        }
        SystemUserDTO systemUserDTO = systemUserService.getStarPointByUid(uid);
        return ApiResult.success(systemUserDTO);
    }
}

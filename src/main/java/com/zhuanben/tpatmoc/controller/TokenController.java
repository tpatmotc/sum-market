package com.zhuanben.tpatmoc.controller;

import com.zhuanben.tpatmoc.domain.ApiResult;
import com.zhuanben.tpatmoc.service.TokenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(tags="token2")
@RestController
@RequestMapping("/token")
public class TokenController {

    @Resource
    TokenService tokenService;

    @ApiOperation("token刷新接口")
    @PostMapping("/refresh")
    @PreAuthorize("permitAll()")
    public ApiResult refereshToken(@RequestBody String refreshToken){
//        System.out.println("刷新了token");
        Map response = tokenService.refreshNewToken(refreshToken);
        return ApiResult.success(response);
    }
}

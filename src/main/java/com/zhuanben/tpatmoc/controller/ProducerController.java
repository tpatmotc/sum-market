package com.zhuanben.tpatmoc.controller;

import com.zhuanben.tpatmoc.config.RabbitMQConfig;
import com.zhuanben.tpatmoc.domain.ApiResult;
import com.zhuanben.tpatmoc.service.QueueMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(value = "/provider")
public class ProducerController {
    @Autowired
    private QueueMessageService RabbitMqQueueMessageService;


    /**
     *
     * @return
     */
    @Deprecated
    @PostMapping("/send")
    @PreAuthorize("false")
    public ApiResult sendMessage() {
        try {
            Map<String, Object> messageMap = new HashMap<>();
            messageMap.put("messageId", UUID.randomUUID().toString());
            messageMap.put("messageData","测试Test信息");
            messageMap.put("createTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            RabbitMqQueueMessageService.send(messageMap,
                                    RabbitMQConfig.EXCHANGE_TOPICS,
                                    RabbitMQConfig.ROUTING_KEY_ORDER_CANCEL);
            return ApiResult.success("succ");
        } catch (Exception e) {
//            System.out.println("ProducerController--err"+e);
            return ApiResult.errorMsg("err");
        }
    }
}
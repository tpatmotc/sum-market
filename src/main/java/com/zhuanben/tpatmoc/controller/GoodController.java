package com.zhuanben.tpatmoc.controller;

import com.alibaba.fastjson.JSON;
import com.zhuanben.tpatmoc.domain.*;
import com.zhuanben.tpatmoc.domain.dto.GoodDTO;
import com.zhuanben.tpatmoc.domain.dto.GoodItemDTO;
import com.zhuanben.tpatmoc.service.GoodDetailService;
import com.zhuanben.tpatmoc.service.GoodIntroImgService;
import com.zhuanben.tpatmoc.service.GoodMainImgService;
import com.zhuanben.tpatmoc.service.GoodService;
import com.zhuanben.tpatmoc.util.RedisUtils;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Api(tags="商品详细信息")
@RestController
@RequestMapping("/good")
public class GoodController {

    @Resource
    GoodService goodService;

    @Resource
    GoodDetailService goodDetailService;

    @Resource
    GoodMainImgService goodMainImgService;

    @Resource
    GoodIntroImgService goodIntroImgService;



    /**
     * 获取一个Good商品的头部信息（标题，主图，最低价格）
     * @param goodId
     * @return
     */
    @ApiOperation(value = "商品列表",response = GoodItemDTO.class)
    @GetMapping("/item/{goodId}")
    @PreAuthorize("permitAll()")
    public ApiResult getGoodItemByGoodId(@PathVariable("goodId") Long goodId){
        GoodItemDTO goodItemDTO = new GoodItemDTO();
//        goodItemDTO.setGood(goodService.getGoodByGoodId(goodId));
        goodItemDTO.setMainImgPath(goodMainImgService.getOneMainImgByGoodId(goodId));
        return ApiResult.success(JSON.toJSONString(goodItemDTO));
    }


    /**
     * 通过分类id来获取商品item
     * @param cateId
     * @return
     */
    @ApiOperation(value = "根据分类查找商品信息",response =GoodItemDTO.class)
    @GetMapping("/category/{categoryId}")
    @PreAuthorize("permitAll()")
    public ApiResult getGoodItemByCategoryId(@PathVariable("categoryId")  Long cateId){
        List<GoodItemDTO> goodItemDtoList = goodService.getGoodByCategoryId(cateId);
//        System.out.println(goodItemDtoList);
        return ApiResult.success(goodItemDtoList);
    }


    @ApiOperation(value = "查找所有商品",response =GoodItemDTO.class)
    @Deprecated
    @GetMapping("/goods")
    @PreAuthorize("permitAll()")
    public ResponseEntity getAllGoods(){
        return null;
    }


    @ApiOperation(value="根据id查找商品详情",response =GoodItemDTO.class)
    @GetMapping("/detail/{goodId}")
    @PreAuthorize("permitAll()")
    public ResponseEntity getGoodDetailByGoodId(@PathVariable("goodId") Long goodId){
        GoodDTO goodDTO = goodService.getGoodDetailByGoodId(goodId);
        return new ResponseEntity(goodDTO, HttpStatus.OK);
    }

    @ApiOperation(value = "根据字段搜索指定字段商品",response =GoodItemDTO.class)
    @GetMapping("/search/{keyword}")
    @PreAuthorize("permitAll()")
    public ApiResult searchGood(@PathVariable("keyword") String keyword){
        List<GoodItemDTO> goodItemDTOList = goodService.searchByKeyword(keyword);
         return ApiResult.success(goodItemDTOList);
    }

    @GetMapping("/recommend")
    @PreAuthorize("permitAll()")
    public ApiResult recommendYouLike(){
        List<GoodItemDTO> goodItemDTOList = goodService.recommendYourLike();
        return ApiResult.success(goodItemDTOList);
    }
}

package com.zhuanben.tpatmoc.controller;

import com.zhuanben.tpatmoc.domain.ApiResult;
import com.zhuanben.tpatmoc.domain.Category;
import com.zhuanben.tpatmoc.domain.dto.GoodItemDTO;
import com.zhuanben.tpatmoc.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
//import javafx.stage.StageStyle;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(tags="产品分类")
@RestController
public class CategoryController {
    @Resource
    CategoryService categoryService;

    @ApiOperation(value="产品一级分类")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查找成功",response = Category.class),
            @ApiResponse(code = 500, message = "未知错误，请联系管理员"),
    })
    @GetMapping("category")
    @PreAuthorize("permitAll()")
    public ApiResult getCategoryLevelOne(){
        Map<String, List> levelOneCategory = new HashMap<>();
        levelOneCategory.put("levelOne", categoryService.getCategoryOne());
        return ApiResult.success(levelOneCategory);
    }

    @ApiOperation(value="产品子级分类",response = Category.class)
    @GetMapping("category/{parentCategoryId}")
    @PreAuthorize("permitAll()")
    public ApiResult getCategoryLevel(@PathVariable("parentCategoryId") int parentCategoryId){
        ApiResult response = ApiResult.success(categoryService.getChildCategory(parentCategoryId));
        return response;
    }
}

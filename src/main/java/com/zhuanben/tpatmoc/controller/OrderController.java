package com.zhuanben.tpatmoc.controller;

import com.zhuanben.tpatmoc.customizeException.IllegalRequestException;
import com.zhuanben.tpatmoc.customizeException.PurchaseException.StockNotEnoughException;
import com.zhuanben.tpatmoc.domain.ApiResult;
import com.zhuanben.tpatmoc.domain.dto.OrderDTO;
import com.zhuanben.tpatmoc.service.OrderDetailService;
import com.zhuanben.tpatmoc.service.OrderService;
import com.zhuanben.tpatmoc.util.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(tags="订单")
@RestController
@RequestMapping("/order")
public class OrderController {

    @Resource
    OrderService orderService;

    @Resource
    OrderDetailService orderDetailService;



    /**
     * 创建订单
     * @author 余志豪
     * @param orderDTO 构造的 订单DTO对象
     * @return
     */
    @ApiOperation(value="创建订单")
    @PostMapping("/orders")
    @PreAuthorize("hasAnyRole('ROLE_normal')")
    public ApiResult createOrder(@RequestBody OrderDTO orderDTO) {

        // 验证是否是正常的请求
        if (!JwtUtils.verifyTokenAndUidIsSame(orderDTO.getOrderUid())) {
            throw new IllegalRequestException("非法请求");
        }
        try{
//            System.out.println("here");
            Long orderId = orderService.createOneOrder(orderDTO);
//            System.out.println("here3333");
            if(orderId>0){
                Map<String, String> map = new HashMap<>();
                map.put("orderId", orderId.toString());
//                System.out.println(map);
//                System.out.println("here222");
                return ApiResult.success(map);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }catch (StockNotEnoughException e){
            throw e;
        }
//        System.out.println("here2");
        return ApiResult.errorMsg("购买失败，请重试");
    }

    /** 确认支付
     * 必填OrderId, orderUid, PayTime(毫秒数)
     * @param
     * @return
     */
    @PostMapping("/orders/payment")
    @PreAuthorize("hasRole('ROLE_normal')")
    public ApiResult confirmPay(@RequestBody OrderDTO orderDTO) throws Exception {
        // 验证是否是正常的请求
        if (!JwtUtils.verifyTokenAndUidIsSame(orderDTO.getOrderUid())) {
            throw new IllegalRequestException("非法请求");
        }
        if(orderService.confirmPay(orderDTO)){
            return ApiResult.success("付款成功");
        }
        return ApiResult.errorMsg("服务器繁忙，付款失败",ApiResult.SERVER_ERROR);
    }

    /**
     * 必填orderId, orderUid
     */
    @DeleteMapping("/orders/payment")
    @PreAuthorize("hasRole('ROLE_normal')")
    public ApiResult cancelOrder(@RequestBody OrderDTO orderDTO){
        // 验证是否是正常的请求
        if (!JwtUtils.verifyTokenAndUidIsSame(orderDTO.getOrderUid())) {
            throw new IllegalRequestException("非法请求");
        }

        if(orderService.cancelOrderByOrderId(orderDTO.getOrderId())){
            return ApiResult.success("订单取消成功");
        }
        return ApiResult.errorMsg("服务器繁忙，请寻找客服或稍后再试");
    }

    /**
     * @author 余志豪
     * @param uid 传入的userId
     * @return 回传 订单DTO 的对象
     */
    @ApiOperation(value="查找所有订单(查找全部订单时不需要填写state,查找未付款等的订单时需要填写state)",response = OrderDTO.class)
    @GetMapping("/orders/{uid}")
    @PreAuthorize("hasAnyRole('ROLE_normal')")
    public ApiResult getAllOrderByUid(@PathVariable("uid") Long uid){
        // 验证是否是正常的请求
        if (!JwtUtils.verifyTokenAndUidIsSame(uid)) {
            throw new IllegalRequestException("非法请求");
        }
        List<OrderDTO> allOrder = orderService.getAllOrderByUidAndState(uid,null);

        return ApiResult.success(allOrder);
    }

    /**
     *
     * @param uid
     * @param state 订单状态数字
     * @return 包含指定订单DTO的数据
     */
    @ApiOperation(value="根据id和状态查找指定订单",response = OrderDTO.class)
    @GetMapping("/orders/{uid}/{state}")
    @PreAuthorize("hasAnyRole('ROLE_normal')")
    public ApiResult getOrderByUidAndState(@PathVariable("uid") Long uid,
                                                @PathVariable("state") Integer state){
        // 验证是否是正常的请求
        if (!JwtUtils.verifyTokenAndUidIsSame(uid)) {
            throw new IllegalRequestException("非法请求");
        }

        List<OrderDTO> allOrder = orderService.getAllOrderByUidAndState(uid, state);
        return ApiResult.success(allOrder);
    }

    /**
     * 获取订单的详细信息
     * @param orderId
     * @param uid
     * @return
     */
    @GetMapping("/order/{orderId}/{uid}")
    @PreAuthorize("hasRole('ROLE_normal')")
    public ApiResult getOrderDetailByOrderId(@PathVariable("orderId") Long orderId,
                                             @PathVariable("uid") Long uid){
        // 验证是否是正常的请求
        if (!JwtUtils.verifyTokenAndUidIsSame(uid)) {
            throw new IllegalRequestException("非法请求");
        }
        OrderDTO orderThoroughInfo = orderService.getOrderThoroughInfoByOrderId(orderId);
        return ApiResult.success(orderThoroughInfo);
    }

    /**
     * 对订单的子订单进行退款
     */
    @ApiOperation("用于订单内的子订单的退款")
    @DeleteMapping("/order/{orderDetailId}/{uid}")
    @PreAuthorize("hasAnyRole('ROLE_normal')")
    public ApiResult cancelOrderDetailByOrderDetailId(@PathVariable("orderDetailId")Long orderDetailId,
                                                @PathVariable("uid")Long uid){
        // 验证是否是正常的请求
        if (!JwtUtils.verifyTokenAndUidIsSame(uid)) {
            throw new IllegalRequestException("非法请求");
        }
//        System.out.println("调用cancelOrderDetailByOrderDetailId");
        if (orderDetailService.cancelOrderDetailByOrderDetailId(orderDetailId)) {
            return ApiResult.success("退款成功");
        }
        return ApiResult.errorMsg("退款失败，请稍后重试或联系客服");
    }

    @ApiOperation("订单确认收货,必填orderUid, finishTime, orderId")
    @PostMapping("/order")
    @PreAuthorize("hasRole('ROLE_normal')")
    public ApiResult confirmReceive(@RequestBody OrderDTO orderDTO){
        // 验证是否是正常的请求
        if (!JwtUtils.verifyTokenAndUidIsSame(orderDTO.getOrderUid())) {
            throw new IllegalRequestException("非法请求");
        }
//        System.out.println(orderDTO);
        if (orderService.confirmReceiveByOrderId(orderDTO.getOrderId(),
                orderDTO.getOrderFinishTime().getTime())) {
            return ApiResult.success("收货成功");
        }
        return null;
    }
}

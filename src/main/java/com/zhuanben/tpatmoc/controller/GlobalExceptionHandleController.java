package com.zhuanben.tpatmoc.controller;


import com.zhuanben.tpatmoc.customizeException.TokenException.TokenInvalidException;
import com.zhuanben.tpatmoc.customizeException.TokenException.TokenTimeoutException;
import com.zhuanben.tpatmoc.domain.ApiResult;
import com.zhuanben.tpatmoc.domain.LoginUser;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;


@Api(tags="全局异常处理Handler")
@RestController
public class GlobalExceptionHandleController {

    /**
     * @usage 捕捉token出错的Handler
     * @author yzh
     * @param request
     * @return 直接返回实体类到前端
     */
    @RequestMapping("/exception/token")
    public ApiResult TokenExceptionHandler(HttpServletRequest request) {
        Object tokenException = request.getAttribute("tokenException");
        // 处理token格式不正确
        if(tokenException instanceof TokenInvalidException){
            throw (TokenInvalidException) tokenException;
        // 处理token过期
        } else if (tokenException instanceof TokenTimeoutException) {
            ((TokenTimeoutException) tokenException).getMessage();
            throw (TokenTimeoutException)tokenException;
        }
        return null;
    }
}

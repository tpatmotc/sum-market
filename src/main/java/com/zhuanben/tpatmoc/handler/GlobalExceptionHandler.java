package com.zhuanben.tpatmoc.handler;

import com.zhuanben.tpatmoc.customizeException.BaseException;
import com.zhuanben.tpatmoc.customizeException.IllegalRequestException;
import com.zhuanben.tpatmoc.customizeException.PurchaseException.PurchaseException;
import com.zhuanben.tpatmoc.customizeException.PurchaseException.StockNotEnoughException;
import com.zhuanben.tpatmoc.customizeException.TokenException.TokenInvalidException;
import com.zhuanben.tpatmoc.customizeException.TokenException.TokenTimeoutException;
import com.zhuanben.tpatmoc.customizeException.WechatException.WechatException;
import com.zhuanben.tpatmoc.domain.ApiResult;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 *  //@ControllerAdvice注解作用
 *  增强的 Controller。使用这个 Controller ，可以实现三个方面的功能：
 *       1.全局异常处理；
 *       2.全局数据绑定
 *       3.全局数据预处理
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 处理非法token
     * @param exception
     * @return
     */
    @ExceptionHandler(TokenInvalidException.class)
    @ResponseBody
    public ApiResult tokenInvalidExceptionHandle(TokenInvalidException exception) {
        return ApiResult.errorMsg(exception.getMessage(), ApiResult.UNAUTHORIZED);
    }

    /**
     * 处理token超时
     * @param exception
     * @return
     */
    @ExceptionHandler(TokenTimeoutException.class)
    @ResponseBody
    public ApiResult tokenTimeoutExceptionHandle(TokenTimeoutException exception) {
        return ApiResult.errorMsg(exception.getMessage(), ApiResult.UNAUTHORIZED);
    }

    /**
     * 处理微信登录有关的异常
     * @return
     */
    @ExceptionHandler(WechatException.class)
    @ResponseBody
    public ApiResult invalidWechatRequestHandler(){
        return ApiResult.errorMsg("非法的微信请求",ApiResult.FORBIDDEN);
    }

    /**
     * 处理购买的异常
     * @param e
     * @return
     */
    @ExceptionHandler(PurchaseException.class)
    @ResponseBody
    public ApiResult PurchaseExceptionHandler(Exception e) {
        /* 处理库存不足的异常 */
        if(e instanceof StockNotEnoughException){
            return ApiResult.errorMsg(e.getMessage());
        }
        return ApiResult.errorMsg("购买出错，请联系客服");
    }

    /**
     * 对非法请求的响应
     */
    @ExceptionHandler(IllegalRequestException.class)
    @ResponseBody
    public ApiResult IllegalRequestExceptionHandler(IllegalRequestException  e){
        return ApiResult.errorMsg("非法请求", ApiResult.CLIENT_ERROR);
    }
}
